<?php
namespace Model;

class DuplicateEntryException extends \Exception {}
class InvalidStateException extends \Exception {}
class WrongPasswordException extends \Exception {}
class CircleDoesNotExistsException extends \Exception {}

