<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Skalda
 */
class Utils {
    static function path2url($file_path, $protocol='http://') {
	$file_path=str_replace('\\','/',$file_path);
	$file_path=str_replace('app/../', '', $file_path);
	if(isset($_SERVER['DOCUMENT_ROOT'])) {
	    $file_path=str_replace($_SERVER['DOCUMENT_ROOT'],'',$file_path);
	}
       if(isset($_SERVER['HTTP_HOST'])) {
	    $file_path=$protocol.$_SERVER['HTTP_HOST'].$file_path;
       }
	return $file_path;
    }
}