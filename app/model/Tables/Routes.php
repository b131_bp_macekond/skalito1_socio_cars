<?php

namespace Model;

/**
 * @author Tomáš Skalický
 */
class Routes extends Table
{
	static public $routecolors = array(
		'blue' => array(
		    'id' => 'blueLine',
		    'color' => 'ffff0000',
		),
		'red' => array(
		    'id' => 'redLine',
		    'color' => 'ff0000ff',
		),
		'green' => array(
		    'id' => 'greenLine',
		    'color' => 'ff009900',
		),
		'orange' => array(
		    'id' => 'orangeLine',
		    'color' => 'ff00ccff',
		),
		'pink' => array(
		    'id' => 'pinkLine',
		    'color' => 'ffff33ff',
		),
		'brown' => array(
		    'id' => 'brownLine',
		    'color' => 'ff66a1cc',
		),
		'purple' => array(
		    'id' => 'purpleLine',
		    'color' => 'ffcc00cc',
		),
		'yellow' => array(
		    'id' => 'yellowLine',
		    'color' => 'ff61f2f2',
		),
		
	    );
	protected $tableName = 'routes';
	
	/**
	 * Creates new route
	 * 
	 * @param int $vehicleId id of vehicle
	 * @param int $userId id of user
	 * @return \Nette\Database\Table\ActiveRow created row
	 */
	public function addRoute($vehicleId, $userId) {
		return $this->createRow(array(
			'vehicles_id' => $vehicleId,
			'users_id' => $userId,
		));
	}

	/**
	 * Add information to route about length and duration
	 * 
	 * @param type $id id of route
	 * @param type $start time of route's start
	 * @param type $end time of route's end
	 * @param type $start_point coords where route starts
	 * @param type $end_point coords where route ends
	 * @return void
	 */
	public function initRoute($id, $start, $end, $start_point, $end_point) {
		$row = $this->find($id);
		$starttime = \Nette\DateTime::from($start);
		$endtime = \Nette\DateTime::from($end);
		$duration = $endtime->diff($starttime);
		
		$route = $this->connection->table('entries')->where('routes_id', $id)->order('timestamp')->select('latitude, longitude');
		$distance = $this->calculateRouteDistance($route);
		
		$row->update(array(
			'start_time' => $starttime,
			'duration' => $duration->format('%h:%i:%s'),
			'length' => $distance,
			'start_long' => $start_point[0],
			'start_lat' => $start_point[1],
			'end_long' => $end_point[0],
			'end_lat' => $end_point[1],
		));
	}
	
	/*public function update() {
	    $routes = $this->findAll();
	    foreach($routes as $route) {
		$ro = $this->connection->table('entries')->where('routes_id', $route->id)->order('timestamp')->select('latitude, longitude');
		$distance = $this->calculateRouteDistance($ro);
		$r = $this->find($route->id);
		$r->update(array(
		    'length' => $distance
		));
	    }
	}*/
	
	
	/**
	 * Calculates length of route
	 * 
	 * @param array $points array of all coords of route in right order
	 * @return double length of route in km
	 */
	public function calculateRouteDistance($points) {
	    $p = array();
	    foreach($points as $point) {
		$p[] = array($point->latitude, $point->longitude);
	    }
	    $distance = 0;
	    for($i = 0; count($p) > $i+1; $i++) {
		$distance += $this->calculateDistanceBetweenTwoPoints($p[$i], $p[$i+1]);
		//dump(implode(",", $p[$i]) . " - " . implode(",", $p[$i+1]) ." = ".$distance);
	    }
	    
	    return $distance;
	}
	
	/**
	 * Haversine algorithmus
	 * 
	 * @param type $point1 coords of first point
	 * @param type $point2 coords of second point
	 * @return double distance between two points in km
	 */
	public function calculateDistanceBetweenTwoPoints($point1, $point2) {
	    $rlat = deg2rad($point2[0]-$point1[0]);
	    $rlon = deg2rad($point2[1]-$point1[1]);
	    $a = sin($rlat/2) * sin($rlat/2) +
		    cos(deg2rad($point1[0])) *
		    cos(deg2rad($point2[0])) *
		    sin($rlon/2) * sin($rlon/2);
	    $c = 2 * atan2(sqrt($a), sqrt(1-$a));
	    $d = 6367 * $c;
	    return $d;
	}

	/**
	 * Gets all vehicle's route
	 * 
	 * @param int $vehicleId id of vehicle
	 * @return \Nette\Database\Table\Selection Vehicle's routes
	 */
	public function getVehiclesRoutes($vehicleId){
		return $this->findBy(array('vehicles_id' => $vehicleId))->order('start_time ASC');
	}

	/**
	 * Gets all users's route
	 * 
	 * @param int $userId id of user
	 * @return \Nette\Database\Table\Selection Users's routes
	 */
	public function getUsersRoutes($userId){
		$routeUsers = $this->connection->table('route_users')->where('users_id', $userId);
	    $helper = array();
	    foreach($routeUsers as $row) $helper[] = $row['routes_id'];
	    $results1 = iterator_to_array($this->getTable()->where('id', $helper));
	    $results2 = iterator_to_array($this->getTable()->where('users_id', $userId));
	    $rents = $this->connection->table('vehicles')->select(':vehicle_rent.vehicle_id, :vehicle_rent.from, :vehicle_rent.to')->where(array(':vehicle_rent.user_id' => $userId));
	    $routes = array();
	    foreach($rents as $rent) {
		$rs = $this->getTable()->where(array('vehicles_id' => $rent->vehicle_id))->where('start_time BETWEEN ? AND ?', array($rent->from, $rent->to));
		$routes = array_merge($routes, iterator_to_array($rs));
	    }
	    $uses = $this->connection->table('vehicles')->select(':vehicle_drivers.vehicle_id');
	    $usesroutes = array();
	    foreach($uses as $use) {
		$rs = $this->getTable()->where(array('vehicles_id' => $use->vehicle_id));
		$usesroutes = array_merge($usesroutes, iterator_to_array($rs));
	    }
	    return array_unique(array_merge($results1, $results2, $routes, $usesroutes));
	}

	/**
	 * Changes info about route
	 * 
	 * @param int $id id of route
	 * @param string $name name of route
	 * @param int $vehicles_id id of used vehicle
	 * @param array $sharers array of ids of users who share route
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function modifyRoute($id, $name, $vehicles_id, $sharers) {
		$row = $this->find($id)->update(array(
			'name' => $name,
			'vehicles_id' => $vehicles_id,
		));
		$this->connection->table('route_users')->where('routes_id', $id)->delete();
		foreach($sharers as $sharer) $this->connection->table('route_users')->insert(array(
			'routes_id' => $id,
			'users_id' => $sharer,
	    ));
		return $row;
	}

	/**
	 * Gets route's waypoints
	 * 
	 * @param int $id id of route
	 * @return \Nette\Database\Table\Selection Route's waypoints
	 */
	public function getRoutePoints($id) {
	    return $this->connection->table('entries')->select("latitude, longitude")->where('routes_id', $id);
	}
	
	
	/**
	 * Gets route's entries
	 * 
	 * @param int $routeId id of route
	 * @return \Nette\Database\Table\Selection Route's entries
	 */
	public function getRoutesEntries($routeId){
		return $this->connection->table('entries')->where(array('routes_id' => $routeId))->order('timestamp ASC');
	}
	
	/**
	 * Gets route's stats
	 * 
	 * @param int $routeId id of route
	 * @return \Nette\Database\Table\Selection Route's stats
	 */
	public function getRoutesStats($routeId) {
	    $route = $this->getRoutesEntries($routeId)->select('AVG(engine_rpm) AS engine_rpm, AVG(consumption) AS consumption, MAX(consumption) AS max_consumption, AVG(velocity) AS velocity, MAX(velocity) AS max_velocity')->fetch();
	    return $route;
	}
	
	/**
	 * Gets similar routes
	 * 
	 * @param int $id id of route
	 * @return array similar routes
	 */
	public function getSimilarRoutes($id) {
	    $route = $this->getRoute($id);
	    $args = array(
		$route->start_lat,
		$route->start_lat,
		$route->start_long,
		$route->end_lat,
		$route->end_lat,
		$route->end_long,
		$route->users_id,
		$route->id,
	    );
	    $results = $this->connection->queryArgs("SELECT *, " .
					"(" . 
					    "6367* 2 * ASIN (".
						"SQRT (" . 
						    "POWER(SIN((start_lat - ?)*pi()/180 / 2),2) + " . 
						    "COS(start_lat * pi()/180) * " .
						    "COS(? *pi()/180) * " .
						    "POWER(SIN((start_long - ?) *pi()/180 / 2), 2) " .
						") ".
					    ")" . 
					") as start_distance, " .
					"(" . 
					    "6367* 2 * ASIN (".
						"SQRT (" . 
						    "POWER(SIN((end_lat - ?)*pi()/180 / 2),2) + " . 
						    "COS(end_lat * pi()/180) * " .
						    "COS(? *pi()/180) * " .
						    "POWER(SIN((end_long - ?) *pi()/180 / 2), 2) " .
						") ".
					    ")" . 
					") as end_distance " . 
					"FROM routes " .
					"WHERE users_id = ? AND id <> ? ".
					"HAVING start_distance < 0.5 and end_distance < 0.5 ", $args);
	    return $results->fetchAll();
	}
	
	/**
	 * Generates new KML file and return URL to this file
	 * 
	 * @param array $ids array of route to show id=>color
	 * @return string URL to KML file
	 */
	public function createKML($ids) {
	    $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><kml></kml>');
	    $xml->addAttribute('xmlns', 'http://www.opengis.net/kml/2.2');
	    $document = $xml->addChild('Document');
	    foreach(self::$routecolors as $color) {
		$style = $document->addChild('Style');
		$style->addAttribute('id', $color['id']);
		$lineStyle = $style->addChild('LineStyle');
		$lineStyle->addChild('color', $color['color']);
		$lineStyle->addChild('width', '4');
	    }
	    foreach($ids as $id=>$color) {
		$route = $this->getRoute($id);
		$points = $this->getRoutePoints($id);
		$placemark = $document->addChild('Placemark');
		if($route->name != null) {
		    $placemark->addChild('name', $route->name);
		}
		$placemark->addChild('styleUrl', '#'.self::$routecolors[$color]['id']);
		$lineString = $placemark->addChild('LineString');
		$pts = array();
		foreach($points as $point) {
		    $pts[] = round($point['longitude'],5) . ',' . round($point['latitude'], 5);
		}
		$coo = $lineString->addChild('coordinates', implode(",0.0\n", $pts) . ',0.0');
	    }
	    $xml->saveXML(WWW_DIR.'/data/routes/'.implode('-',  array_keys($ids)).'.kml');
	    
	    return \Utils::path2url(WWW_DIR.'/data/routes/'.implode('-',array_keys($ids)).'.kml');
	}
	
	/**
	 * Gets single route
	 * 
	 * @param int $id id of route
	 * @return array Route
	 */
	
	public function getRoute($id) {
		$row = $this->find($id);
		if(!$row) {
			return null;
		}
		return $row;
	}
}