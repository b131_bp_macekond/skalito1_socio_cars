<?php

namespace Model;


/**
 * @author Tomáš Skalický
 */
class FriendsCircles extends Table
{
	/** @var string */
	protected $tableName = 'friends_circles';
	
	/**
	 * Gets user's circles
	 * 
	 * @param int $id user's id
	 * @return \Nette\Database\Table\Selection circles
	 */
	public function getUsersCircles($id) {
	    return $this->findBy(array(
		'user_id' => $id,
	    ));
	}
	
	/**
	 * Gets all friends in selected circle
	 * 
	 * @param int $id id of circle
	 * @return \Nette\Database\Table\Selection users in circle
	 */
	public function getFriendsInCircle($id) {
	    $connect = $this->connection->table('friends_to_circles')->where(array('friends_circle_id' => $id))->fetchPairs('users_id', 'users_id');
	    return $this->connection->table('users')->where('id', array_values($connect));
	}
	
	/**
	 * Gets single circle
	 * 
	 * @param int $id id of circle
	 * @param int $user_id id of circle's owner
	 * @return \Nette\Database\Table\ActiveRow circle
	 * @throws CircleDoesNotExistsException thrown when circle not exists
	 */
	public function getSingleCircle($id, $user_id) {
	    $circle = $this->getUsersCircles($user_id)->where(array('id' => $id))->fetch();
	    if($circle == NULL) {
		throw new CircleDoesNotExistsException;
	    }
	    return $circle;
	}
	
	/**
	 * Adds new circle
	 * 
	 * @param int $user_id id of creator
	 * @param string name of circle
	 * @return \Nette\Database\Table\ActiveRow created row
	 */
	public function addCircle($user_id, $name) {
	    return $this->createRow(array(
		'user_id' => $user_id,
		'name' => $name,
	    ));
	}
	
	/**
	 * Edits circle and adds friends to it
	 * 
	 * @param int $id id of circle
	 * @param int $user_id id of creator
	 * @param string $name new name of circle
	 * @param array $friends ids of friends whos is that circle
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function editCircle($id, $user_id, $name, $friends) {
	    $circle = $this->getSingleCircle($id, $user_id);
	    $result = $circle->update(array('name' => $name));
	    $table = $this->connection->table('friends');
	    $res = $table->where('from = ? OR to = ?', array($user_id, $user_id))->where(array('accepted' => 1));
	    $connections = array();
	    foreach($res as $r) {
		$key = ($r['from'] == $user_id)?$r['to']:$r['from'];
		$connections[$key] = $r['id'];
	    }
	    $this->connection->table('friends_to_circles')->where(array('friends_circle_id' => $id))->delete();
	    foreach($friends as $val) {
		if($connections[$val]) {
		    $this->connection->table('friends_to_circles')->insert(array(
			'friend_id'  => $connections[$val],
			'users_id' => $val,
			'friends_circle_id' => $id,
		    ));
		}
		    
	    }
	    return $result;
	}
	
	/**
	 * Deletes circle
	 * 
	 * @param int $id id of circle
	 * @param int $user_id id of creator
	 */
	public function deleteCircle($id, $user_id) {
	    $circle = $this->getSingleCircle($id, $user_id);
	    return $circle->delete();
	}
}

