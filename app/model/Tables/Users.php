<?php

namespace Model;

use Nette\Security;

/**
 * @author Tomáš Skalický
 */
class Users extends Table implements Security\IAuthenticator
{
	/** @var string */
	protected $tableName = 'users';
	
	const NON_FRIEND = 0,
		FRIEND = 1,
		AUTHOR = 2;
	
	const ALL = 0,
		FRIENDS_ONLY = 1,
		CIRCLES = 2;
	
	/**
	 * Creates new user
	 * 
	 * @param string $email email
	 * @param string $password password
	 * @throws \Model\DuplicateEntryException
	 * @return \Nette\Database\Table\ActiveRow created row
	 */
	public function addUser($email, $password) {
		$salt = sha1(time().$email."ad~632as@!oa");
		$password = $this->getPasswordHash($password, $salt);
			$user = $this->createRow(array(
			    'email' => $email,
			    'password' => $password,
			    'salt' => $salt,
			));
			return $user;
	}
	
	/**
	 * Changes user's password
	 * 
	 * @param int $id id of user
	 * @param string $oldPassword old password
	 * @param string $newPassword new password
	 * @return \Nette\Database\Table\ActiveRow updated row
	 * @throws WrongPasswordException
	 */
	public function changePassword($id, $oldPassword, $newPassword) {
		$row = $this->find($id);
		if($row->password !== $this->getPasswordHash($oldPassword, $row->salt)) {
		    throw new WrongPasswordException('Staré heslo není správné.');
		}
		$password = $this->getPasswordHash($newPassword, $row->salt);
		return $row->update(array(
		    'password' => $password,
		));
	}
	
	/**
	 * Changes user's detail
	 * 
	 * @param int $id id of user
	 * @param string $name name
	 * @param string $surname surname
	 * @param string(male|female) $sex sex
	 * @param city $city city
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function modifyUser($id, $name, $surname, $sex, $city) {
	    $row = $this->find($id);
	    return $row->update(array(
			'name' => $name,
			'surname' => $surname,
			'sex' => $sex,
			'city' => $city,
			'filled_data' => 1
	    ));
	}
	
	/**
	 * Changes user's profile picture
	 * 
	 * @param int $id id of user
	 * @param string $pic profile picture filename
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function changeProfilePic($id, $pic) {
	    $row = $this->find($id);
	    return $row->update(array(
			'profilpic' => $pic,
	    ));
	}
	
	/**
	 * Gets info about user
	 * 
	 * @param int $id id of user
	 * @return array User's info
	 */
	public function getUser($id) {
	    $row = $this->find($id);
	    if(!$row) {
			return null;
	    }
	    $data = $row->toArray();
	    unset($data['password'], $data['salt']);
	    return $data;
	}
	
	
	/**
	 * Gets info about user by email and password
	 * 
	 * @param string $email user's email
	 * @param string $password user's password
	 * @return boolean
	 */
	public function getUserByEmailAndPassword($email, $password) {
	    $row = $this->findOneBy(array('email' => $email));
	    if(!$row) {
			return false;
	    }
	    if($row->password !== $this->getPasswordHash($password, $row->salt)) {
		return false;
	    }
	    return $row;
	}
	
	/**
	 * Checks if password is right for selected user
	 * 
	 * @param int $id user's id
	 * @param string $password user's password
	 * @return boolean
	 */
	public function verifyUsersPassword($id, $password) {
	    $row = $this->find($id);
	    if(!$row) {
			return false;
	    }
	    return $row->password === $this->getPasswordHash($password, $row->salt);
	}
	
	/**
	 * Finds all user which contains words in $q
	 * 
	 * @param string $q search query 
	 * @return \Nette\Database\Table\Selection Found users
	 */
	public function searchUser($q) {
	    $words = preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/", $q, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
	    $results = $this->getTable();
	    foreach($words as $w) {
			$query = "%".$w."%";
			$results->where('email LIKE ? OR name LIKE ? OR surname LIKE ?', array($query, $query ,$query));
	    }
	    return $results;
	}
	
	/**
	 * Gets user's wallposts
	 * 
	 * @param int $id id of user
	 * @param int $viewer viewer's friend status
	 * @return \Nette\Database\Table\Selection User's wallposts
	 */
	public function getUsersWall($id, $viewer = self::NON_FRIEND, $user_id = null) {
	    $user = $this->find($id);
	    $wallpost = $user->related('wall')->order('date DESC');
	    if($viewer == self::NON_FRIEND) {
		$wallpost = $wallpost->where(array('privacy' => 0));
	    } elseif($viewer == self::FRIEND) {
		$wallpost = $this->filterPostsByCircles($wallpost, $user_id);
	    }
	    return $wallpost;
	}
	
	/**
	 * Filters posts by circles user is member of
	 * @param array|\Nette\Database\Table\Selection $posts posts to filter
	 * @param int $user_id id of user the post are filtered for
	 * @return array filtered posts
	 */
	private function filterPostsByCircles($posts, $user_id) {
	    $wallposts = array();
		foreach($posts as $wall) {
		    if($wall['privacy'] == 2) {
			$circles = $this->getCirclesUsersIsIn($user_id, $wall['user_id'])->fetchAll();
			$postCircles = $this->connection->table('wall_to_circles')->where(array('wall_id' => $wall['id']))->fetchPairs('friends_circle_id', 'friends_circle_id');
			$bool = false;
			foreach($circles as $circle) {
			    if(in_array($circle['id'], $postCircles)) {
				$bool = true;
			    }
			}
			if($bool == true) {
			    $wallposts[] = $wall;
			}
		    } else {
			$wallposts[] = $wall;
		    }
		}
		return $wallposts;
	}
	
	/**
	 * Gets wallposts of user's friends
	 * 
	 * @param array $friends array of user's friends ids
	 * @return \Nette\Database\Table\Selection Friends wallposts
	 */
	public function getFriendsWallPosts($friends, $user_id = null) {
	    $wallposts = $this->connection->table('wall')->where('user_id', $friends)->order('date DESC');
	    $wallposts = $this->filterPostsByCircles($wallposts, $user_id);
	    return $wallposts;
	}
	
	/**
	 * Gets posts from multiple groups
	 * 
	 * @param array $groups ids of groups
	 * @return array Groups posts
	 */
	
	public function getGroupPosts($groups) {
	    $result = array();
	    $group_wall = $this->connection->table('groups_wall')->where('group_id', $groups)->order('date DESC');
	    foreach($group_wall as $post) {
		$result[] = $post;
		
	    }
	    return $result;
	}
	
	/**
	 * Gets all posts (from groups and friends) for user sorted by date
	 * 
	 * @param array $friends ids of friends
	 * @param array $groups ids of groups
	 * @param int $user_id id of user posts are got for
	 * @return type
	 */
	public function getFeedPosts($friends, $groups, $user_id) {
	    $friends_wall = $this->getFriendsWallPosts($friends, $user_id);
	    $group_wall = $this->getGroupPosts($groups);
	    $wallposts = $this->uniteFeed($friends_wall, $group_wall);
	    
	    return $wallposts;
	}
	
	/**
	 * Unites two feeds sorted by date
	 * 
	 * @param array|\Nette\Database\Table\Selection $feed1 first feed
	 * @param array|\Nette\Database\Table\Selection $feed2 second feed
	 * @return array united feeds
	 */
	private function uniteFeed($feed1, $feed2) {
	    $pos1 = 0;
	    $pos2 = 0;
	    $result = array();
	    $val1 = null;
	    $val2 = null;
	    $size1 = count($feed1);
	    $size2 = count($feed2);
	    do {
		$val1 = null;
		$val2 = null;
		if($pos1 >= $size1) $pos1 = -1;
		if($pos2 >= $size2) $pos2 = -1;
		if($pos1 != -1) {
		    $val1 = $feed1[$pos1]->date;
		}
		if($pos2 != -1) {
		    $val2 = $feed2[$pos2]->date;
		}
		if($pos1 != -1 || $pos2 != -1) {
		    if($val1 > $val2) {
			$result[] = $feed1[$pos1];
			$pos1++;
		    } else {
			$result[] = $feed2[$pos2];
			$pos2++;
		    }
		}
	    } while($pos1 != -1 || $pos2 != -1);
	    return $result;
	    
	}
	
	/**
	 * Gets circles for single post
	 * 
	 * @param int $id id of post
	 * @return \Nette\Database\Table\Selection post's circles
	 */
	public function getWallPostCircles($id) {
	    return $this->connection->table('wall_to_circles')->where(array('wall_id' => $id));
	}
	
	/**
	 * Gets all of friend's circles users is in 
	 * 
	 * @param int $user_id id of user
	 * @param int $friend_id id of friend
	 * @return \Nette\Database\Table\Selection circles
	 */
	public function getCirclesUsersIsIn($user_id, $friend_id) {
	    $result = $this->connection->table('friends_circles')->select('friends_circles.id, name')->where(array(
		'user_id' => $friend_id,
		':friends_to_circles.users_id' => $user_id,
	    ));
	    return $result;
	}
	
	/**
	 * Gets single wallpost
	 * 
	 * @param int $id id of wallpost
	 * @param int $viewer viewer's friend status
	 * @return \Nette\Database\Table\ActiveRow Wallpost
	 */
	public function getSingleWallPost($id, $viewer = self::NON_FRIEND) {
	    $wallpost = $this->connection->table('wall')->wherePrimary($id);
	    if($viewer == self::NON_FRIEND) {
			$wallpost = $wallpost->where(array('privacy' => 0));
	    }
	    return $wallpost;
	}
	
	/**
	 * Adds wallpost to user's wall
	 * 
	 * @param int $id id of user
	 * @param string $content content of wallpost
	 * @param int $privacy privacy settings
	 * @return \Nette\Database\Table\ActiveRow created row
	 */
	public function addWallPost($id, $content, $privacy = self::ALL, $route = null, $circles = array()) {
	    $wall = $this->connection->table('wall');
	    $return = $wall->insert(array(
			'user_id' => $id,
			'content' => $content,
			'privacy' => $privacy,
			'routes_id' => $route,
			'date' => new \Nette\Database\SqlLiteral('NOW()'),
	    ));
	    
	    if($privacy == self::CIRCLES) {
		foreach($circles as $circle) {
		    $this->connection->table('wall_to_circles')->insert(array(
			'wall_id' => $return->id,
			'friends_circle_id' => $circle,
		    ));
		}
	    }
	    
	    return $return;
	}
	
	/**
	 * Adds comment to users's wallpost
	 * 
	 * @param int $userId id of comment's author
	 * @param int $wallpostId id of wallpost
	 * @param string $content content of the comment
	 * @return \Nette\Database\Table\ActiveRow created row
	 */
	public function addComment($userId, $wallpostId, $content) {
	    $comments = $this->connection->table('comments');
	    return $comments->insert(array(
		'user_id' => $userId,
		'wall_id' => $wallpostId,
		'content' => $content,
		'date' => new \Nette\Database\SqlLiteral('NOW()'),
	    ));
	}
	
	/**
	 * Performs an authentication.
	 * 
	 * @param array $credentials array of users credentials array(email, password)
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;
		$row = $this->findOneBy(array('email' => $email));

		if (!$row) {
			throw new Security\AuthenticationException('Uživatelské jméno neexistuje.', self::IDENTITY_NOT_FOUND);
		}
		
		if ($row->password !== $this->getPasswordHash($password, $row->salt)) {
			throw new Security\AuthenticationException('Heslo není správné.', self::INVALID_CREDENTIAL);
		}

		$arr = $row->toArray();
		unset($arr['password'], $arr['salt']);
		return new Security\Identity($row->id, 1, $arr);
	}
	
	/**
	 * Creates password hash
	 * 
	 * @param string $password password
	 * @param string $salt salt
	 * @return string Hashed password
	 */
	public function getPasswordHash ($password, $salt) {
		return sha1($password . "are14%!u@#raia" . $salt);
	}
        
}
