<?php

namespace Model;

/**
 * @author Tomáš Skalický
 */
class Vehicles extends Table
{
	/** @var string */
	protected $tableName = 'vehicles';
	
	/**
	 * Creates new vehicle
	 * 
	 * @param int $id id of owner
	 * @param array $info all informations about vehicle
	 * @return \Nette\Database\Table\ActiveRow created row
	 */
	public function addVehicle($id, $info) {
		return $this->createRow(array(
			'users_id' => $id,
			/*'unit_id' => ($info['unit_id'])?:null,*/
			'name' => $info['name'],
			'desc' => ($info['desc'])?:null,
			'registration_number' => $info['registration_number'],
			'type' => $info['type'],
			'vehicle_make' => $info['vehicle_make'],
			'vehicle_type' => $info['vehicle_type'],
			'commercial_description' => $info['commercial_description'],
			'vin' => $info['vin'],
			'mass_permissible' => $info['mass_permissible'],
			'mass_operating' => $info['mass_operating'],
			'engine_capacity' => $info['engine_capacity'],
			'engine_max_net_power' => ($info['engine_max_net_power'])?:null,
			'engine_fuel' => $info['engine_fuel'],
			'seating' => $info['seating'],
			'stands' => $info['stands'],
			'color' => ($info['color'])?:null,
			'power_weigth' => ($info['power_weigth'])?:null,
			'maximum_speed' => ($info['maximum_speed'])?:null,
		));
	}

	/**
	 * Gets all user's vehicles
	 * 
	 * @param int $userId id of user
	 * @return \Nette\Database\Table\Selection User's vehicles
	 */
	public function getUsersVehicles($userId){
		return $this->findBy(array('users_id' => $userId))->where('active', 1)->order('name DESC');
	}

	/**
	 * Changes info about vehicle
	 * 
	 * @param type $id id of vehicle
	 * @param array $info all informations about vehicle
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function modifyVehicle($id, $info) {
		$row = $this->find($id);
		return $row->update(array(
			/*'unit_id' => ($info['unit_id'])?:null,*/
			'name' => $info['name'],
			'desc' => ($info['desc'])?:null,
			'registration_number' => $info['registration_number'],
			'type' => $info['type'],
			'vehicle_make' => $info['vehicle_make'],
			'vehicle_type' => $info['vehicle_type'],
			'commercial_description' => $info['commercial_description'],
			'vin' => $info['vin'],
			'mass_permissible' => $info['mass_permissible'],
			'mass_operating' => $info['mass_operating'],
			'engine_capacity' => $info['engine_capacity'],
			'engine_max_net_power' => ($info['engine_max_net_power'])?:null,
			'engine_fuel' => $info['engine_fuel'],
			'seating' => $info['seating'],
			'stands' => $info['stands'],
			'color' => ($info['color'])?:null,
			'power_weigth' => ($info['power_weigth'])?:null,
			'maximum_speed' => ($info['maximum_speed'])?:null,
		));
	}
	
	/**
	 * Gets vehicle state (0 for inactive, 1 for active)
	 * 
	 * @param int $id id of vehicle
	 * @return int vehicle state
	 */
	public function getVehiclesState($id) {
	    return $this->find($id)->active;
	}
	
	/**
	 * Change vehicle's state to active
	 * 
	 * @param int $id id of vehicle
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function makeVehicleActive($id) {
	    $row = $this->find($id);
		return $row->update(array(
			'active' => 1,
		));
	}
	
	/**
	 * Change vehicle's state to inactive
	 * 
	 * @param int $id id of vehicle
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function makeVehicleInactive($id) {
	    $row = $this->find($id);
		return $row->update(array(
			'active' => 0,
		));
	}

	/**
	 * Change vehicle's picture
	 * 
	 * @param int $id id of vehicle
	 * @param string $pic profile picture filename
	 * @return type
	 */
	public function changeVehiclePic($id, $pic) {
		$row = $this->find($id);
		return $row->update(array(
			'profilpic' => $pic,
		));
	}

	/**
	 * Changes vehicle's owner
	 * 
	 * @param int $id id of vehicle
	 * @param int $userId id of new owner
	 * @return \Nette\Database\Table\ActiveRow updated row
	 */
	public function changeOwner($id, $userId) {
		return $this->find($id)->update(array(
			'users_id' => $userId,
		));
	}

	/**
	 * Gets single vehicle info
	 * 
	 * @param int $id id of vehicle
	 * @return \Nette\Database\Table\ActiveRow Vehicle info
	 */
	public function getVehicle($id) {
		$row = $this->find($id);
		if(!$row) {
			return null;
		}
		return $row;
	}
	
	/**
	 * Gets all available types of vehicle
	 * 
	 * @return \Nette\Database\Table\Selection Types of vehicle
	 */
	public function getVehiclesTypes() {
	    $row = $this->connection->table('vehicle_type')->order('id');
	    return $row;
	}
	
	/**
	 * Gets all available vehicle's make
	 * 
	 * @return \Nette\Database\Table\Selection Vehicle's make
	 */
	public function getVehiclesMake() {
	    $row = $this->connection->table('vehicle_make')->order('name');
	    return $row;
	}
	
	/**
	 * Gets all drivers of vehicle
	 * 
	 * @param int $id id of vehicle
	 * @return \Nette\Database\Table\Selection Drivers of vehicle
	 */
	public function getDrivers($id) {
	    $results = $this->connection->table('vehicle_drivers')->where(array('vehicle_id' => $id));
	    return $results;
	}
	
	/**
	 * Adds user as driver of vehicle
	 * 
	 * @param int $id id of vehicle
	 * @param int $user_id id of user
	 * @return \Nette\Database\Table\ActiveRow created row
	 */
	public function addDriver($id, $user_id) {
	    return $this->connection->table('vehicle_drivers')->insert(array(
		'user_id' => $user_id,
		'vehicle_id' => $id,
	    ));
	}
	
	/**
	 * Removes driver of vehicle
	 * 
	 * @param int $id id of driver
	 * @return \Nette\Database\Table\ActiveRow
	 */
	public function removeDriver($id) {
	    return $this->connection->table('vehicle_drivers')->wherePrimary($id)->delete();
	}
	
	/**
	 * Gets all vehicles rents
	 * 
	 * @param int $id id of vehicle
	 * @return \Nette\Database\Table\Selection vehicles rents
	 */
	public function getRent($id) {
	    $results = $this->connection->table('vehicle_rent')->where(array('vehicle_id' => $id));
	    return $results;
	}
	
	/**
	 * Creates new vehicles rent
	 * 
	 * @param int $id id of vehicle
	 * @param int $user_id id of user 
	 * @param \DateTime|string $from time when rent becomes active
	 * @param \DateTime|string $to time when rent stops being active
	 * @return \Nette\Database\Table\ActiveRow created row
	 * @throws AlreadyExistDateRangeException thrown when vehicle is already rented during select time
	 */
	public function addRent($id, $user_id, $from, $to) {
	    if(!$this->isRentPossible($id, $from, $to)) {
		throw new AlreadyExistDateRangeException;
	    }
	    return $this->connection->table('vehicle_rent')->insert(array(
		'user_id' => $user_id,
		'vehicle_id' => $id,
		'from' => $from,
		'to' => $to,
	    ));
	}
	
	/**
	 * Returns if rent in selected time is possible
	 * 
	 * @param int $id id of vehicle
	 * @param \DateTime|string $from time when rent becomes active
	 * @param \DateTime|string $to time when rent stops being active
	 * @return boolean is rent possible
	 */
	public function isRentPossible($id, $from, $to) {
	    $results = $this->connection->table('vehicle_rent')->where(array('vehicle_id' => $id))->where('(from BETWEEN ? AND ?) OR (to BETWEEN ? AND ?)', array($from, $to, $from, $to));
	    return $results->count() == 0;
	}
	
	/**
	 * Gets vehicles rent active on selected day
	 * 
	 * @param int $id id of vehicle
	 * @param \DateTime|string $date selected day
	 * @return \Nette\Database\Table\Selection vehicles rent
	 */
	public function getRentByDate($id, $date) {
	    $results = $this->connection->table('vehicle_rent')->where(array('vehicle_id' => $id))->where('(from <= ? AND to >= ?)', array($date, $date));
	    return $results;
	}
	
	/**
	 * Gets users active rented vehicle
	 * 
	 * @param int $user_id id of user
	 * @return \Nette\Database\Table\Selection active rented vehicle
	 */
	public function getRentedVehicles($user_id) {
	    $results = $this->connection->table('vehicles')->where(array(':vehicle_rent.user_id' => $user_id))->where('now() BETWEEN :vehicle_rent.from AND :vehicle_rent.to')->where('active', 1);
	    return $results;
	}
	
	/**
	 * Gets users inactive rented vehicle
	 * 
	 * @param int $user_id id of user
	 * @return \Nette\Database\Table\Selection inactive rented vehicle
	 */
	public function getOldRentedVehicles($user_id) {
	    $results = $this->connection->table('vehicles')->where(array(':vehicle_rent.user_id' => $user_id))->where('now() > :vehicle_rent.to');
	    return $results;
	}
	
	/**
	 * Gets users vehicle for which he is mark as driver
	 * 
	 * @param int $user_id id of user
	 * @return \Nette\Database\Table\Selection vehicles 
	 */
	public function getUsesVehicles($user_id) {
	    $results = $this->connection->table('vehicles')->where(array(':vehicle_drivers.user_id' => $user_id))->where('active', 1);
	    return $results;
	}
	
	/**
	 * Gets users vehicle which are mark as inactive
	 * 
	 * @param int $user_id id of user
	 * @return \Nette\Database\Table\Selection inactive vehicles 
	 */
	public function getInactiveVehicles($user_id) {
	    $results = $this->connection->table('vehicles')->where(array('users_id' => $user_id, 'active'=> 0));
	    return $results;
	}
}

class AlreadyExistDateRangeException extends \Exception {};