-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `wall_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `wall_id` (`wall_id`),
  CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `comments_ibfk_4` FOREIGN KEY (`wall_id`) REFERENCES `wall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `entries`;
CREATE TABLE `entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `routes_id` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `odometer` double DEFAULT NULL,
  `velocity` double NOT NULL,
  `consumption` double NOT NULL,
  `fuel_remaining` double NOT NULL,
  `altitude` double NOT NULL,
  `engine_temp` double NOT NULL,
  `engine_rpm` double NOT NULL,
  `throttle` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `routes_id` (`routes_id`),
  CONSTRAINT `entries_ibfk_3` FOREIGN KEY (`routes_id`) REFERENCES `routes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_from_user_to` (`from`,`to`),
  KEY `user_to` (`to`),
  CONSTRAINT `friends_ibfk_3` FOREIGN KEY (`from`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `friends_ibfk_4` FOREIGN KEY (`to`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `friends_circles`;
CREATE TABLE `friends_circles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `friends_circles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `friends_to_circles`;
CREATE TABLE `friends_to_circles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friend_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `friends_circle_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `friend_id` (`friend_id`),
  KEY `friends_circle_id` (`friends_circle_id`),
  KEY `users_id` (`users_id`),
  CONSTRAINT `friends_to_circles_ibfk_3` FOREIGN KEY (`friend_id`) REFERENCES `friends` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friends_to_circles_ibfk_4` FOREIGN KEY (`friends_circle_id`) REFERENCES `friends_circles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friends_to_circles_ibfk_6` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `groups_comments`;
CREATE TABLE `groups_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `groups_wall_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `groups_wall_id` (`groups_wall_id`),
  CONSTRAINT `groups_comments_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `groups_comments_ibfk_4` FOREIGN KEY (`groups_wall_id`) REFERENCES `groups_wall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `groups_members`;
CREATE TABLE `groups_members` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `groups_members_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `groups_members_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `groups_wall`;
CREATE TABLE `groups_wall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `groups_wall_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `groups_wall_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `link` varchar(255) NOT NULL,
  `link_params` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `attr` text NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `route_users`;
CREATE TABLE `route_users` (
  `routes_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  KEY `routes_id` (`routes_id`),
  KEY `users_id` (`users_id`),
  CONSTRAINT `route_users_ibfk_4` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `route_users_ibfk_5` FOREIGN KEY (`routes_id`) REFERENCES `routes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `routes`;
CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicles_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `length` double DEFAULT NULL,
  `start_lat` double DEFAULT NULL,
  `start_long` double DEFAULT NULL,
  `end_lat` double DEFAULT NULL,
  `end_long` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicles_id` (`vehicles_id`),
  KEY `users_id` (`users_id`),
  CONSTRAINT `routes_ibfk_4` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `routes_ibfk_5` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `sex` enum('male','female') DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `profilpic` varchar(255) DEFAULT NULL,
  `filled_data` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `vehicle_drivers`;
CREATE TABLE `vehicle_drivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_id` (`vehicle_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `vehicle_drivers_ibfk_1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `vehicle_drivers_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `vehicle_make`;
CREATE TABLE `vehicle_make` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `vehicle_make` (`id`, `name`) VALUES
(1,	'Abarth'),
(2,	'AC'),
(3,	'Acura'),
(4,	'Aixam'),
(5,	'Alfa Romeo'),
(6,	'Alpina'),
(7,	'AM General Truck'),
(8,	'ARO'),
(9,	'Asia Motors'),
(10,	'Aston Martin'),
(11,	'Audi'),
(12,	'Austin'),
(13,	'Auverland'),
(14,	'Bellier'),
(15,	'Bentley'),
(16,	'BMW'),
(17,	'Bugatti'),
(18,	'Buick'),
(19,	'Cadillac'),
(20,	'Casalini'),
(21,	'Caterham'),
(22,	'Changhe'),
(23,	'Chatenet'),
(24,	'Chevrolet'),
(25,	'Chrysler'),
(26,	'Citroën'),
(27,	'Dacia'),
(28,	'Daewoo'),
(29,	'Daihatsu'),
(30,	'Dodge'),
(31,	'Eagle'),
(32,	'Ferrari'),
(33,	'Fiat'),
(34,	'Fisker'),
(35,	'Ford'),
(36,	'GAZ'),
(37,	'GMC'),
(38,	'Gonow'),
(39,	'Gordon'),
(40,	'GREAT WALL'),
(41,	'Grecav'),
(42,	'Honda'),
(43,	'Honker'),
(44,	'Hummer'),
(45,	'Hurtan'),
(46,	'Hyundai'),
(47,	'Infiniti'),
(48,	'Isuzu'),
(49,	'Italcar'),
(50,	'Iveco'),
(51,	'Jaguar'),
(52,	'JDM'),
(53,	'Jeep'),
(54,	'Kaipan'),
(55,	'Kia'),
(56,	'Koenigsegg'),
(57,	'Lada'),
(58,	'Lamborghini'),
(59,	'Lancia'),
(60,	'Land Rover'),
(61,	'Lexus'),
(62,	'Ligier'),
(63,	'Lincoln'),
(64,	'Lotus'),
(65,	'Marcos'),
(66,	'Martin Motors'),
(67,	'Maruti'),
(68,	'Maserati'),
(69,	'Mazda'),
(70,	'MCC'),
(71,	'McLaren'),
(72,	'Mercedes-Benz'),
(73,	'Mercury'),
(74,	'MG'),
(75,	'Microcar'),
(76,	'Mini'),
(77,	'Mitsubushi'),
(78,	'Morgan'),
(79,	'Moskvič'),
(80,	'MTX'),
(81,	'Nissan'),
(82,	'Oldsmobile'),
(83,	'Oltcit'),
(84,	'Opel'),
(85,	'Peugeot'),
(86,	'Plymouth'),
(87,	'Polski Fiat'),
(88,	'Pontiac'),
(89,	'Porsche'),
(90,	'Proton'),
(91,	'Renault'),
(92,	'Rolls Royce'),
(93,	'Rover'),
(94,	'Saab'),
(95,	'Santana'),
(96,	'Saturn'),
(97,	'Scion'),
(98,	'Seat'),
(99,	'Smart'),
(100,	'SsangYong'),
(101,	'Subaru'),
(102,	'Suzuki'),
(103,	'Škoda'),
(104,	'Tata'),
(105,	'Tatra'),
(106,	'Tavria'),
(107,	'Tesla'),
(108,	'Toyota'),
(109,	'Trabant'),
(110,	'Triumph'),
(111,	'TVR'),
(112,	'UAZ'),
(113,	'Ultima'),
(114,	'Volkswagen'),
(115,	'Volvo'),
(116,	'Wartburg'),
(117,	'Yugo'),
(118,	'Zastava'),
(119,	'Ostatní');

DROP TABLE IF EXISTS `vehicle_rent`;
CREATE TABLE `vehicle_rent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `vehicle_id` (`vehicle_id`),
  CONSTRAINT `vehicle_rent_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `vehicle_rent_ibfk_2` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `vehicle_type`;
CREATE TABLE `vehicle_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `vehicle_type` (`id`, `name`) VALUES
(1,	'Osobní'),
(2,	'Terénní'),
(3,	'Užitkové'),
(4,	'Nákladní'),
(5,	'Autobus'),
(6,	'Obytné'),
(7,	'Přívěs'),
(8,	'Motorka'),
(9,	'Ostatní'),
(10,	'Pracovní stroj');

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `profilpic` varchar(255) DEFAULT NULL,
  `desc` text,
  `registration_number` varchar(7) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `vehicle_make` int(11) DEFAULT NULL,
  `vehicle_type` varchar(255) NOT NULL,
  `commercial_description` varchar(255) NOT NULL,
  `vin` varchar(255) NOT NULL,
  `mass_permissible` int(11) NOT NULL,
  `mass_operating` int(11) NOT NULL,
  `engine_capacity` int(11) NOT NULL,
  `engine_max_net_power` varchar(255) DEFAULT NULL,
  `engine_fuel` varchar(255) NOT NULL,
  `seating` int(11) NOT NULL,
  `stands` int(11) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `power_weigth` varchar(255) DEFAULT NULL,
  `maximum_speed` varchar(255) DEFAULT NULL,
  `mileage` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  KEY `type` (`type`),
  KEY `vehicle_make` (`vehicle_make`),
  CONSTRAINT `vehicles_ibfk_3` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vehicles_ibfk_4` FOREIGN KEY (`type`) REFERENCES `vehicle_type` (`id`) ON DELETE SET NULL,
  CONSTRAINT `vehicles_ibfk_5` FOREIGN KEY (`vehicle_make`) REFERENCES `vehicle_make` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `wall`;
CREATE TABLE `wall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `content` text NOT NULL,
  `routes_id` int(11) DEFAULT NULL,
  `privacy` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `routes_id` (`routes_id`),
  CONSTRAINT `wall_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `wall_ibfk_3` FOREIGN KEY (`routes_id`) REFERENCES `routes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `wall_to_circles`;
CREATE TABLE `wall_to_circles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wall_id` int(11) NOT NULL,
  `friends_circle_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wall_id` (`wall_id`),
  KEY `friends_circle_id` (`friends_circle_id`),
  CONSTRAINT `wall_to_circles_ibfk_1` FOREIGN KEY (`wall_id`) REFERENCES `wall` (`id`),
  CONSTRAINT `wall_to_circles_ibfk_2` FOREIGN KEY (`friends_circle_id`) REFERENCES `friends_circles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP VIEW IF EXISTS `feed`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `feed` AS select `wall`.`id` AS `id`,NULL AS `group_id`,`wall`.`user_id` AS `user_id`,`wall`.`date` AS `date`,`wall`.`content` AS `content`,`wall`.`privacy` AS `privacy`,'user' AS `type` from `wall` union select `groups_wall`.`id` AS `id`,`groups_wall`.`group_id` AS `group_id`,`groups_wall`.`user_id` AS `user_id`,`groups_wall`.`date` AS `date`,`groups_wall`.`content` AS `content`,NULL AS `privacy`,'group' AS `type` from `groups_wall`;

-- 2014-05-22 21:29:29