<?php
namespace Form;

/**
 * @author Tomáš Skalický
 */
class VehicleRentForm extends Base
{
	
	protected function buildForm() {
		$this->addSelect('user', 'Uživateli:')
		    ->setPrompt('Zvolte uživatele')
		    ->setRequired('Zvolte uživatele.');
		$this->addDatePicker('from', 'Od:')
		    ->addRule(\Nette\Forms\Form::FILLED, 'Musíte vyplnit od kdy')
		    ->addRule(\Nette\Forms\Form::VALID, 'Datum musí být validní');
		$this->addDatePicker('to', 'Do:')
		    ->addRule(\Nette\Forms\Form::FILLED, 'Musíte vyplnit do kdy')
		    ->addRule(\Nette\Forms\Form::VALID, 'Datum musí být validní');
		$this->addHidden('vehicle_id');
		$this->addSubmit('send', 'Půjčit');
	}
}