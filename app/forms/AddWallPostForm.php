<?php
namespace Form;

/**
 * @author Tomáš Skalický
 */
class AddWallPostForm extends Base
{
	
	protected function buildForm() {
		$this->addTextArea('content', NULL, 100, 5)
		    ->setRequired('Vložte obsah příspěvku.');
		
		$this->addSelect('route', 'Zobrazit trasu:')->setPrompt('-- Žádná --');
		$this->addSelect('privacy', 'Zobrazit:')
			->setItems(array(
			    0 => 'Všem',
			    1 => 'Pouze přátelům',
			    2 => 'Vybrat kruhy',
			))
			->setDefaultValue(0);
		$this->addMultiSelect('circles', 'Kruhy:');
		$this->addSubmit('send', 'Vložit');
	} 
}
