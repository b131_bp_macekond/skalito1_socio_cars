<?php
namespace Form;

/**
 * @author Tomáš Skalický
 */
class CircleForm extends Base
{
	
	protected function buildForm() {
		$this->addText('name', 'Název:')
		    ->setRequired('Vložte název kruhu.');
		$this->addSubmit('submit', 'Přidat kruh');
	}
}