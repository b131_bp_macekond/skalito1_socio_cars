<?php
namespace Form;

use	Nette;

/**
 * @author Tomáš Skalický
 */
abstract class Base extends \Nette\Application\UI\Form 
{
	/** @var \GettextTranslator\Gettext */
	protected $translator;
	
	public function __construct(Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		parent::__construct($parent, $name);
		\Nette\Forms\Container::extensionMethod('addDatePicker', function (\Nette\Forms\Container $container, $name, $label = NULL) {
		    return $container[$name] = new \JanTvrdik\Components\DatePicker($label);
		});
		$element = $this->getElementPrototype();
		$element->addAttributes(array('role' => 'form'));
		/** @var \Nette\Forms\Rendering\DefaultFormRenderer */
		$renderer = $this->getRenderer();
		if($renderer instanceof Nette\Forms\Rendering\DefaultFormRenderer) {
			$renderer->wrappers['controls']['container'] = NULL;
			$renderer->wrappers['pair']['container'] = \Nette\Utils\Html::el('div', array('class'=>'form-group'));
			$renderer->wrappers['control']['container'] = null;
			$renderer->wrappers['control']['.text'] = 'form-control';
			$renderer->wrappers['control']['.password'] = 'form-control';
			$renderer->wrappers['control']['.submit'] = 'btn btn-default';
			$renderer->wrappers['control']['.button'] = 'btn btn-default';
			$renderer->wrappers['label']['container'] = null;
		}
		$this->setTranslator($this->translator);
		$this->buildForm();
	}
	
	/**
	 * Abstract function which handles the form creation.
	 * @abstract
	 * @return void
	 */
	protected abstract function buildForm();
}
