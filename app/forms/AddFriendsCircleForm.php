<?php
namespace Form;

/**
 * @author Tomáš Skalický
 */
class AddFriendsCircleForm extends Base
{
	
	protected function buildForm() {
		$this->addText('circle', 'Kruh')
		    ->setRequired('Vložte název kruhu.');
		$this->addSubmit('send', 'Vytvořit');
	}
}