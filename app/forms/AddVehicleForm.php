<?php
namespace Form;

class AddVehicleForm extends Base
{
    /** @var \Model\Vehicles */
    private $vehicle;
    
    public function __construct(\Model\Vehicles $vehicle) {
	$this->vehicle = $vehicle;
	parent::__construct();
    }
    
	protected function buildForm() {
		$this->addText('name', 'Název:')
			->setRequired('Vložte název vozidla.');
		$this->addTextArea('desc', 'Popis:');
		/*$this->addText('unit_id', 'ID palubní jednotky:');*/
		$this->addText('registration_number', 'Registrační značka vozidla:')
			->setRequired('Vyplňte registrační značku vozidla.')
			->addCondition(self::FILLED)
			->addRule(self::LENGTH, 'Registrační číslo musí mít délku 7 znaků.', 7);
		$this->addSelect('type', 'Typ:', $this->vehicle->getVehiclesTypes()->fetchPairs('id', 'name'))->setPrompt('-- Vyberte --')
			->setRequired('Vyberte typ vozidla.');
		$this->addSelect('vehicle_make', 'Značka:', $this->vehicle->getVehiclesMake()->fetchPairs('id', 'name'))->setPrompt('-- Vyberte --')
			->setRequired('Vyplňte tovární značku.');
		$this->addText('vehicle_type', 'Typ vozidla (varianta, verze):')
			->setRequired('Vyplňte typ vozidla.');
		$this->addText('commercial_description', 'Model:')
			->setRequired('Vyplňte obchodní označení');
		$this->addText('vin', 'Identifikační číslo vozidla (VIN):')
			->setRequired('Vyplňte identifikační číslo vozidla.');
		$this->addText('mass_permissible', 'Nejvyšší technicky přípustná hmotnost:')
			->setRequired('Vyplňte nejvyšší technicky přípustnou hmotnost.');
		$this->addText('mass_operating', 'Provozní hmotnost:')
			->setRequired('Vyplňte provozní hmotnost.');
		$this->addText('engine_capacity', 'Zdvihový objem (v cm3):')
			->setRequired('Vyplňte zdvihový objem.');
		$this->addText('engine_max_net_power', 'Maximální výkon:');
		$this->addText('engine_fuel', 'Palivo:')
			->setRequired('Vyplňte palivo.');
		$this->addText('seating', 'Počet míst k sezení:')
			->setRequired('Vyplňte počet míst k sezení.');
		$this->addText('stands', 'Počet míst k stání:')
			->setRequired('Vyplňte počet míst k stání.');
		$this->addText('color', 'Barva:');
		$this->addText('power_weigth', 'Poměr výkon/hmotnost:');
		$this->addText('maximum_speed', 'Nejvyšší rychost:');

		$this->addSubmit('send', 'Přidat');
	}
}
