<?php
namespace Form;

/**
 * @author Tomáš Skalický
 */
class VehicleDriversForm extends Base
{
	
	protected function buildForm() {
		$this->addSelect('user', 'Uživateli:')
		    ->setPrompt('Zvolte uživatele')
		    ->setRequired('Zvolte uživatele.');
		$this->addHidden('vehicle_id');
		$this->addSubmit('send', 'Půjčit');
	}
}