<?php

/**
 *
 * @author Tomáš Skalický
 */
class VehiclesPresenter extends BasePresenter
{
	/** @var Model\Vehicles @inject */
	public $vehicles;
	
	public function renderDefault()
	{
	    if(!$this->getUser()->isLoggedIn())
	    	$this->redirect('Homepage:');
	    $this->template->vehicleslist = $this->vehicles->getUsersVehicles($this->getUser()->getId());
	    $this->template->useslist = $this->vehicles->getUsesVehicles($this->getUser()->getId());
	    $this->template->rentedVehicles = $this->vehicles->getRentedVehicles($this->getUser()->getId());
	    $this->template->oldRentedVehicles = $this->vehicles->getOldRentedVehicles($this->getUser()->getId());
	    $this->template->inactiveVehicles = $this->vehicles->getInactiveVehicles($this->getUser()->getId());
	}

	public function createComponentAddVehicleForm() {
	    if(!$this->getUser()->isLoggedIn())
		    return null;
	    $form = new \Form\AddVehicleForm($this->vehicles);
	    $form->onSuccess[] = $this->addVehicleSucceeded;
	    return $form;
	}
	
	public function addVehicleSucceeded($form) {
	    $values = $form->getValues();
	    
	    $this->vehicles->addVehicle($this->getUser()->getId(), $values);
	    $this->flashMessage('Vehicle added.', 'success');
	    $this->redirect('Vehicles:');

	    return $form;
	}
}