<?php

/**
 *
 * @author Tomáš Skalický
 */
class RoutePresenter extends BasePresenter
{
	/** @var \Model\Users @inject */
	public $users;

	/** @var \Model\Routes @inject */
	public $routes;

	/** @var \Model\Vehicles @inject */
	public $vehicles;

	/** @var \Model\Friends @inject */
	public $friends;

	/** @var \Model\RouteUsers @inject */
	public $routeUsers;

	/** @var \Nette\Database\Table\Selection */
	public $routeId;
	
	public function beforeRender() {
	    $this->template->routeId = $this->routeId;
	    parent::beforeRender();
	}
    
	public function actionDefault($id){
		$this->routeId = $id;
	}
	
	public function renderDefault($id){
		$route = $this->routes->find($id);
		$this->template->similarRoutes = $this->routes->getSimilarRoutes($id);
		if(count($route) == 0) $this->redirect('Homepage:');
		$user = $this->users->getUser($route->users_id);
		$this->template->routeinfo = $route;
		$rent = $this->vehicles->getRentByDate($route->vehicles_id, $route->start_time);
		if($rent->count() == 0) {
		    $driver = $user;
		} else {
		    $f = $rent->fetch();
		    $driver = $f->user;
		}
		$this->template->driver = $driver;
		$this->template->vehiclename = $this->vehicles->getVehicle($route->vehicles_id)['name'];
		$this->template->usermail = $user['email'];
		$this->template->sharers = $this->routeUsers->getRoutesUsers($this->routeId);
		$this->template->routeStats = $this->routes->getRoutesStats($this->routeId);
		if(!isset($this->template->compare)) {
		    $kml = $this->routes->createKML(array($route->id => 'blue'));
		    $this['map']->setKML($kml);
		    $this->template->compare = array($id=>'blue');
		}
	}
	
	public function handleAddRoute($current, $new) {
	    $current = unserialize($current);
	    $used = array_values($current);
	    $possible = \Model\Routes::$routecolors;
	    $use = null;
	    foreach($possible as $color => $val) {
		if(array_search($color, $used) === FALSE) {
		    $use = $color;
		    break;
		}
	    }
	    if($use != null) {
		$compare = $current + array($new => $color);
		$kml = $this->routes->createKML($compare);
		$this['map']->setKML($kml);
		$this->template->compare = $compare;
		$this['map']->invalidateControl();
		$this->invalidateControl('map');
		$this->invalidateControl('compare');
	    }
		 
	}
	
	public function handleDeleteRoute($current, $delete) {
	    $current = unserialize($current);
	    unset($current[$delete]);
	    
	    $kml = $this->routes->createKML($current);
	    $this['map']->setKML($kml);
	    $this->template->compare = $current;
	    $this->invalidateControl('map');
	    $this->invalidateControl('compare');
		 
	}

	public function createComponentMap() {
		$map = new Components\Map();
		return $map;
	}
	
	public function actionModifyRoute($id) {
		$this->routeId = $id;
	}

	public function renderModifyRoute($id) {
		$route = $this->routes->find($id);
		if(count($route) == 0) $this->redirect('Homepage:');
		$sharers = $this->routeUsers->getRoutesUsers($this->routeId)->fetchPairs('id');
		$this['modifyRouteForm']->setDefaults($route);
		$defaults = array();
		foreach($sharers as $key => $val) {
		    $defaults[] = $key;
		}
		$this['modifyRouteForm']['sharers']->setDefaultValue($defaults);
	}
	
	public function createComponentModifyRouteForm() {
	    $form = new Form\ModifyRouteForm();
	    $vehicles = $this->vehicles->getUsersVehicles($this->getUser()->getId())->fetchPairs('id', 'name');
	    $form->addSelect('vehicle', 'Vozidlo:', $vehicles)->setDefaultValue($this->routes->find($this->routeId)->vehicles_id);
	    $friends = $this->friends->getUsersFriends($this->getUser()->getId())->fetchPairs('id', 'email');
	    $form->addMultiSelect('sharers', 'Spolujezdci:', $friends);
	    $form->addSubmit('send', 'Upravit trasu');
	    $form->onSuccess[] = $this->modifyRouteFormSucceeded;
	    return $form;
	}
	
	public function modifyRouteFormSucceeded($form) {
	    $values = $form->getValues();
	    $route = $this->routes->modifyRoute($this->routeId, $values->name, $values->vehicle, $values->sharers);
	    $this->flashMessage('Údaje byly změněny.', 'success');
	    $this->redirect('default', array('id'=>$this->routeId));
	}

	public function actionSharers($id) {
		$this->routeId = $id;
	}

	public function renderSharers($id) {
		$route = $this->routes->find($id);
		if(count($route) == 0) $this->redirect('Homepage:');
		$user = $this->users->getUser($route->users_id);
		$this->template->routeinfo = $route;
		$rent = $this->vehicles->getRentByDate($route->vehicles_id, $route->start_time);
		$this->template->routeStats = $this->routes->getRoutesStats($this->routeId);
		if($rent->count() == 0) {
		    $driver = $user;
		} else {
		    $f = $rent->fetch();
		    $driver = $f->user;
		}
		$this->template->driver = $driver;
		$this->template->vehiclename = $this->vehicles->getVehicle($route->vehicles_id)['name'];
		$this->template->usermail = $user['email'];
		$this->template->sharers = $this->routeUsers->getRoutesUsers($this->routeId);
	}
}