<?php

/**
 *
 * @author Tomáš Skalický
 */
class VehiclePresenter extends BasePresenter
{
	/** @var \Model\Vehicles @inject */
	public $vehicles;

	/** @var \Model\Friends @inject */
	public $friends;

	/** @var \Nette\Database\Table\Selection */
	public $vehicle;

	public function beforeRender() {
	    parent::beforeRender();
	    if(count($this->vehicle) == 0) 
		$this->redirect('Homepage:');
	    $this->template->vehicle = $this->vehicle;
	}
    
	public function actionDefault($id) {
		$this->loadVehicle($id);
	}

	public function actionEditVehicle($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}

	public function renderEditVehicle($id) {
		$this['editVehicleForm']->setDefaults($this->vehicle);
	}
	
	public function createComponentEditVehicleForm() {
	    $form = new Form\AddVehicleForm($this->vehicles);
	    $form->addHidden('id');
	    $form['send']->caption = 'Upravit';
	    $form->onSuccess[] = $this->editVehicleFormSucceeded;
	    return $form;
	}
	
	public function editVehicleFormSucceeded($form) {
	    $values = $form->getValues();
	    $this->vehicles->modifyVehicle($values->id, $values);
	    $this->flashMessage('Údaje byly změněny.', 'success');
	    $this->redirect('default', array('id'=>$values->id));
	}

	public function actionVehiclePic($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}

	public function renderVehiclePic($id) {
	    $this->template->profilPic = $this->vehicle->profilpic;
	}

	public function createComponentImageUploadForm() {
	    $form = new Form\ImageUploadForm();
	    $form->onSuccess[] = $this->imageUploadFormSucceeded;
	    return $form;
	}
	
	public function imageUploadFormSucceeded($form) {
	    $values = $form->getValues();
	    $file = $values->image;
	    if($file->isOk()) {
			$name = $this->vehicle->id . '-' . $file->getSanitizedName();
			$file->move(WWW_DIR.'/data/profil2/'.$name);
			$this->vehicles->changeVehiclePic($this->vehicle->id, $name);
			$this->flashMessage('Fotka vozidla byla změněna.', 'success');
			$this->redirect('this');
	    }
	    $form->addError('Při uploadu nastala chyba, zkuste to prosím znova později.');
	}

	public function actionOwnerChange($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}

	
	public function createComponentOwnerChangeForm() {
	    $form = new Form\OwnerChangeForm();
	    $friends = $this->friends->getUsersFriends($this->getUser()->getId())->fetchPairs('id', 'email');
	    $form['owner']->setItems($friends);
	    $form['id']->setDefaultValue($this->vehicle->id);
	    $form->onSuccess[] = $this->ownerChangeFormSucceeded;
	    return $form;
	}
	
	public function ownerChangeFormSucceeded($form) {
	    $values = $form->getValues();
	    $this->vehicles->changeOwner($values->id, $values->owner);
	    $this->flashMessage('Vozidlo bylo přeneseno k jinému uživateli.', 'success');
	    $this->redirect('Vehicles:');
	}
	
	public function actionState($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}
	
	public function renderState($id) {
	    $this->template->vehiclestate = $this->vehicles->getVehiclesState($id);
	}
	
	public function handleActivate($id) {
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
		$this->vehicles->makeVehicleActive($id);
		$this->redirect('this', array('id'=>$id));
	}
	
	public function handleDeactivate($id) {
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
		$this->vehicles->makeVehicleInactive($id);
		$this->redirect('this', array('id'=>$id));
	}
	
	public function actionDrivers($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}
	
	public function renderDrivers($id) {
	    $this->template->drivers = $this->vehicles->getDrivers($id);
	}
	
	public function handleRemoveDriver($id, $driver_id) {
	    $this->vehicles->removeDriver($driver_id);
	    $this->redirect('this', array('id'=>$id));
	}
	
	public function actionAddDriver($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}
	
	protected function createComponentAddDriverForm() {
	    $form = new \Form\VehicleDriversForm();
	    $form['user']->setItems($this->friends->getUsersFriends($this->getUser()->getId())->fetchPairs('id', 'email'));
	    $form['vehicle_id']->setDefaultValue($this->vehicle->id);
	    $form->onSuccess[] = $this->addDriverFormSucceeded;
	    return $form;
	}
	
	public function addDriverFormSucceeded(\Form\VehicleDriversForm $form) {
	    $values = $form->getValues();
	    $this->vehicles->addDriver($values->vehicle_id, $values->user);
	    $this->flashMessage('Pravidelný řidič byl přidán');
	    $this->redirect('drivers', array('id'=> $values->vehicle_id));
	}
	
	public function actionRent($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}
	
	public function renderRent($id) {
	    $this->template->rents = $this->vehicles->getRent($id);
	}
	
	public function actionAddRent($id) {
		$this->loadVehicle($id);
		if(!$this->isUserOwner($this->getUser()->getId())) {
		    $this->flashMessage('K akci nemáte dostatečné oprávnění.');
		    $this->redirect('default', array('id'=>$id));
		}
	}
	
	protected function createComponentRentVehicleForm() {
	    $form = new \Form\VehicleRentForm();
	    $form['user']->setItems($this->friends->getUsersFriends($this->getUser()->getId())->fetchPairs('id', 'email'));
	    $form['vehicle_id']->setDefaultValue($this->vehicle->id);
	    $form->onSuccess[] = $this->rentVehicleFormSucceeded;
	    return $form;
	}
	
	public function rentVehicleFormSucceeded(\Form\VehicleRentForm $form) {
	    $values = $form->getValues();
	    try {
		$this->vehicles->addRent($values->vehicle_id, $values->user, $values->from, $values->to);
		$this->flashMessage('Vozidlo bylo půjčeno');
		$this->redirect('rent', array('id'=> $values->vehicle_id));
	    } catch (Model\AlreadyExistDateRangeException $e) {
		$form->addError('Vozidlo je v zadaném intervalu již půjčeno.');
	    }
	}
	
	private function loadVehicle($id) {
	    $this->vehicle = $this->vehicles->getVehicle($id);
	}
	
	private function isUserOwner($user_id) {
	    return $this->vehicle->users_id == $user_id;
	}
}