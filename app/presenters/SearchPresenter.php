<?php

/**
 *
 * @author Tomáš Skalický
 */
class SearchPresenter extends BasePresenter
{
	/** @var Model\Users @inject */
	public $users;
	
	/** @var \Model\Groups @inject */
	public $groups;
	
	public function actionDefault($q)
	{
	    $this->redirect('users', array('q' => $q));
	}
	
	public function renderUsers($q)
	{
	    $this->template->results = $this->users->searchUser($q);
	    $this->template->q = $q;
	}
	
	public function renderGroups($q) {
	    $this->template->results = $this->groups->searchGroups($q);
	    $this->template->q = $q;
	}

}
