<?php

/**
 * @author Tomáš Skalický
 */
class FriendsPresenter extends BasePresenter
{
	/** @var Model\FriendsCircles @inject*/
	public $circles;
	
	/** @var Model\Friends @inject*/
	public $friends;
	
	public function startup() {
	    parent::startup();
	    if(!$this->getUser()->isLoggedIn()) {
		$this->redirect('Homepage:');
	    }
	}
	
	public function beforeRender() {
	    parent::beforeRender();
	    $this->template->circles = $this->circles->getUsersCircles($this->getUser()->getId());
	}
	
	public function renderDefault() {
	    $this->template->friends = $this->friends->getUsersFriends($this->getUser()->getId());
	}
	
	public function renderCircle($id) {
	    $circle = $this->circles->getSingleCircle($id, $this->getUser()->getId());
	    if(!$circle) {
		$this->redirect('default');
	    }
	    $this->template->curentCircle = $circle;
	    $this->template->friends = $this->circles->getFriendsInCircle($id);
	}
	
	public function actionEdit($id) {
	    if($id == null)
		$this->redirect('default');
	}
	
	public function renderEdit($id) {
	    $circle = $this->circles->getSingleCircle($id, $this->getUser()->getId());
	    if(!$circle) 
		$this->redirect('default');
	    $friends = $this->friends->getUsersFriends($this->getUser()->getId())->fetchPairs('id');
	    $this['editForm']['friends']->setItems($friends);
	    $this['editForm']->setDefaults($circle);
	    $cir = $this->circles->getFriendsInCircle($id);
	    $defaults = array();
	    foreach($cir as $val) {
		$defaults[] = $val['id'];
	    }
	    $this['editForm']['friends']->setDefaultValue($defaults);
	}
	
	protected function createComponentEditForm() {
	    $form = new \Form\EmptyForm();
	    $form->addText('name', 'Název:')
		->setRequired('Vložte název kruhu.');
	     $friends = $this->friends->getUsersFriends($this->getUser()->getId())->fetchPairs('id');
	    $form->addCheckboxList('friends')->setItems($friends);
	    $form->addSubmit('submit', 'Editovat kruh');
	    $form->addHidden('id');
	    $form->onSuccess[] = $this->editFormSucceeded;
	    return $form;
	}
	
	public function editFormSucceeded(\Form\EmptyForm $form) {
	    $values = $form->getValues();
	    $this->circles->editCircle($values->id, $this->getUser()->getId(), $values->name, $values->friends);
	    $this->flashMessage('Kruh byl editován.');
	    $this->redirect('circle', array('id'=>$values->id));
	}
	
	protected function createComponentAddForm() {
	    $form = new \Form\CircleForm();
	    $form->onSuccess[] = $this->addFormSucceeded;
	    return $form;
	}
	
	public function addFormSucceeded(\Form\CircleForm $form) {
	    $values = $form->getValues();
	    $res = $this->circles->addCircle($this->getUser()->getId(), $values->name);
	    $this->flashMessage('Kruh byl přidán.');
	    $this->redirect('circle', array('id'=>$res->id));
	}
}
