<?php

/**
 *
 * @author Tomáš Skalický
 */
class FeedPresenter extends BasePresenter
{

	/** @var \Model\Users @inject*/
	public $users;
	/** @var \Model\Friends @inject */
	public $friends;
	/** @var \Model\Groups @inject */
	public $groups;
	/** @var \Model\Routes @inject */
	public $routes;
	
	public function actionDefault() {
		if(!$this->getUser()->isLoggedIn()) {
		    $this->redirect('Homepage:');
		}
	}
	
	public function renderDefault()
	{
		$friends = $this->friends->getUsersFriends($this->getUser()->getId());
		$groups = $this->groups->getUsersGroup($this->getUser()->getId());
		$feed = $this->users->getFeedPosts($friends, $groups, $this->getUser()->getId());
		$this->template->wallposts = $feed;
	}
	
	public function createComponentAddCommentForm() {
	    $self = $this;
	    return new \Nette\Application\UI\Multiplier(function ($postId) use ($self) {
		$form = new \Form\AddCommentForm();
		$form['post_id']->setValue($postId);
		$form->onSuccess[] = $self->addCommentFormSucceeded;
		return $form;
	    });
	}
	
	public function addCommentFormSucceeded($form) {
	    if(!$this->getUser()->isLoggedIn()) {
		$this->redirect('this');
	    }
	    $values = $form->getValues();
	    $this->users->addComment($this->getUser()->getId(), $values->post_id, $values->content);
	    $post = $this->users->getSingleWallPost($values->post_id, Model\Users::FRIEND)->fetch();
	    $this->flashMessage('Komentář byl úspěšně přidán.', 'success');
	    if($this->getUser()->getId() != $post->user_id) {
		$this->notifications->addNotification($post->user_id, 'User:default', array('id'=>$post->user_id), 'Uživatel {person} okomentoval váš post', array($this->getUser()->getId()));
	    }
	    $this->redirect('this');
	}
	
	public function createComponentAddGroupCommentForm() {
	    $self = $this;
	    return new \Nette\Application\UI\Multiplier(function ($postId) use ($self) {
		$form = new \Form\AddCommentForm();
		$form['post_id']->setValue($postId);
		$form->onSuccess[] = $self->addGroupCommentFormSucceeded;
		return $form;
	    });
	}
	
	public function addGroupCommentFormSucceeded($form) {
	    if(!$this->getUser()->isLoggedIn()) {
		$this->redirect('this');
	    }
	    $values = $form->getValues();
	    $this->groups->addComment($values->post_id, $this->getUser()->getId(), $values->content);
	    $this->redirect('this');
	}
	
	public function createComponentMap() {
	    $self = $this;
	    return new \Nette\Application\UI\Multiplier(function ($routeId) use ($self) {
		$map = new Components\Map();
		$kml = $self->routes->createKML(array($routeId=>'blue'));
		$map->setKML($kml);
		return $map;
	    });
	}
}
