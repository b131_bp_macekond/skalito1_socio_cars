<?php
/**
 *
 * @author Tomáš Skalický
 */
class ApiPresenter extends BasePresenter {
    /** @var \Model\Routes @inject */
    public $routes;

    /** @var \Model\Users @inject */
    public $users;

    /** @var \Model\Entries @inject */
    public $entries;

    /** @var \Model\Vehicles @inject */
    public $vehicles;
    
    /** @var \Model\Friends @inject */
    public $friends;
    
    public function actionRoute() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	/* { 
	  "unit_id": 12345,
	  "secret_key": "sdfadfsa6as987654754",
	  "entries": [
	  {
	  "location": [14.42067, 50.07598],
	  "timestamp": "2012-10-24T01:00:27.372Z",
	  "event": "UNLOCKED",
	  "user_id": 123,
	  "odometer": 23549.75,
	  "velocity": 0.0,
	  "consumption": 0.0,
	  "fuel_remaining": 35.46,
	  "altitude": 201.46,
	  "engine_temp": 25.3,
	  "engine_rpm": 0.0,
	  "throttle": 0.0
	  },
	  {
	  "location": [14.42065, 50.07599],
	  "timestamp": "2012-10-24T01:03:27.372Z",
	  "user_id": 123,
	  "odometer": 23550,
	  "velocity": 23.631,
	  "consumption": 5.4,
	  "fuel_remaining": 35.46,
	  "altitude": 202.76,
	  "engine_temp": 42.3,
	  "engine_rpm": 2103.5,
	  "throttle": 23.1
	  }
	  ]
	  } */
	$input = file_get_contents('php://input');
	
	$route = json_decode($input,true);
	$car_id = $route["car_id"];
	$user_id = $route["user_id"];
	$password = $route["password"];
	$entries = $route["entries"];
	$user = $this->users->getUser($user_id);
	if (count($user) == 0) {
	    $output = array("status" => "error", "message" => "User with requested user_id doesn't exist.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	if (!$this->users->verifyUsersPassword($user_id, $password)) {
	    $output = array("status" => "error", "message" => "Wrong users password.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	$vehicle = $this->vehicles->getVehicle($car_id);
	if (count($vehicle) == 0) {
	    $output = array("status" => "error", "message" => "Vehicle with requested car_id doesn't exist.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	} else {
	    $newRoute = $this->routes->addRoute($vehicle->id, $vehicle->users_id);
	}
	foreach ($entries as $entry) {
	    $location = $entry["location"];
	    $timestamp = $entry["timestamp"];
	    if(isset($entry["event"])) {
		$event = $entry["event"];
	    } else {
		$event = null;
	    }
	    //$odometer = $entry["odometer"];
	    $odometer = 0;
	    $velocity = $entry["velocity"];
	    //$consumption = $entry["consumption"];
	    $consumption = 0;
	    //$fuel_remaining = $entry["fuel_remaining"];
	    $fuel_remaining = 0;
	    $altitude = $entry["altitude"];
	    $engine_temp = $entry["engine_temp"];
	    $engine_rpm = $entry["engine_rpm"];
	    $throttle = $entry["throttle"];
	    $this->entries->addEntry($newRoute->id, $location, $timestamp, $odometer, $velocity, $consumption, $fuel_remaining, $altitude, $engine_temp, $engine_rpm, $throttle);
	}
	$this->routes->initRoute($newRoute->id, $entries[0]["timestamp"], $entries[count($entries) - 1]["timestamp"], $entries[0]["location"], $entries[count($entries) - 1]["location"]);
	if (json_last_error()) {
	    $output = array("status" => "error", "message" => "Invalid request format (detailed description).");
	} else {
	    $output = array("status" => "ok");
	}
	$output = array("status" => "ok");
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
    public function actionAuth() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	/* { 
	  "email": 12345,
	  "password": "sdfadfsa6as987654754",
	  } */
	$input = file_get_contents('php://input');
	
	$u = json_decode($input,true);
	$email = $u["email"];
	$password = $u["password"];
	$user = $this->users->getUserByEmailAndPassword($email, $password);
	if (!$user) {
	    $output = array("status" => "error", "message" => "User does not exist or password is wrong.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	$output = array(
	    "status" => "ok", 
	    "user" => array(
		"id" => $user['id'],
		"email" => $user['email'],
		"name" => $user['name'],
		"surname" => $user['surname'],
		"sex" => $user['sex'],
		"city" => $user['city'],
	    ));
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
    public function actionUser() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	/* { 
	  "user_id": 12345,
	  } */
	$input = file_get_contents('php://input');
	
	$u = json_decode($input,true);
	$user_id = $u["user_id"];
	$user = $this->users->getUser($user_id);
	if (!$user) {
	    $output = array("status" => "error", "message" => "User does not exist.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	$output = array(
	    "status" => "ok", 
	    "user" => array(
		"email" => $user['email'],
		"name" => $user['name'],
		"surname" => $user['surname'],
		"sex" => $user['sex'],
		"city" => $user['city'],
	    ));
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
    public function actionCars() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	/* { 
	  "user_id": 12345,
	  } */
	$input = file_get_contents('php://input');
	
	$u = json_decode($input,true);
	$user_id = $u["user_id"];
	$vehicles = $this->vehicles->getUsersVehicles($user_id);
	if (count($vehicles) == 0) {
	    $output = array("status" => "error", "message" => "User does not exist or have no car.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	
	$cars = array();
	foreach($vehicles as $info) {
	    $cars[] = array(
		'id' => $info['id'],
		'users_id' => $info['users_id'],
		'name' => $info['name'],
		'desc' => ($info['desc'])?:null,
		'registration_number' => $info['registration_number'],
		'type' => $info['type'],
		'vehicle_make' => $info['vehicle_make'],
		'vehicle_type' => $info['vehicle_type'],
		'commercial_description' => $info['commercial_description'],
		'vin' => $info['vin'],
		'mass_permissible' => $info['mass_permissible'],
		'mass_operating' => $info['mass_operating'],
		'engine_capacity' => $info['engine_capacity'],
		'engine_max_net_power' => ($info['engine_max_net_power'])?:null,
		'engine_fuel' => $info['engine_fuel'],
		'seating' => $info['seating'],
		'stands' => $info['stands'],
		'color' => ($info['color'])?:null,
		'power_weigth' => ($info['power_weigth'])?:null,
		'maximum_speed' => ($info['maximum_speed'])?:null,
	    );
	}
	$output = array(
	    "status" => "ok", 
	    "cars" => $cars,
	);
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
    public function actionCar() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	/* { 
	  "user_id": 12345,
	  } */
	$input = file_get_contents('php://input');
	
	$u = json_decode($input,true);
	$car_id = $u["car_id"];
	$info = $this->vehicles->getVehicle($car_id);
	if (count($info) == 0) {
	    $output = array("status" => "error", "message" => "Car does not exist.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	
	$car = array(
		'id' => $info['id'],
		'users_id' => $info['users_id'],
		'name' => $info['name'],
		'desc' => ($info['desc'])?:null,
		'registration_number' => $info['registration_number'],
		'type' => $info['type'],
		'vehicle_make' => $info['vehicle_make'],
		'vehicle_type' => $info['vehicle_type'],
		'commercial_description' => $info['commercial_description'],
		'vin' => $info['vin'],
		'mass_permissible' => $info['mass_permissible'],
		'mass_operating' => $info['mass_operating'],
		'engine_capacity' => $info['engine_capacity'],
		'engine_max_net_power' => ($info['engine_max_net_power'])?:null,
		'engine_fuel' => $info['engine_fuel'],
		'seating' => $info['seating'],
		'stands' => $info['stands'],
		'color' => ($info['color'])?:null,
		'power_weigth' => ($info['power_weigth'])?:null,
		'maximum_speed' => ($info['maximum_speed'])?:null,
	    );
	
	$output = array(
	    "status" => "ok", 
	    "car" => $car,
	);
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
    public function actionFullFeed() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	
	$input = file_get_contents('php://input');
	
	$u = json_decode($input,true);
	$user_id = $u["user_id"];
	$user = $this->users->getUser($user_id);
	if (!$user) {
	    $output = array("status" => "error", "message" => "User does not exist.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	
	$posts = array();
	
	$feed = $this->users->getUsersWall($user_id, Model\Users::AUTHOR);
	
	foreach($feed as $post) {
	    $comments = array();
	    foreach($post->related('comments')->order('date') as $comment) {
		$comments[] = array(
		    'id' => $comment->id,
		    'author' => $comment->user_id,
		    'date' => $comment->date->format('c'),
		    'text' => $comment->content
		);
	    }
	    $posts[] = array(
		'id' => $post->id,
		'author' => $post->user_id,
		'date' => $post->date->format('c'),
		'text' => $post->content,
		'comments' => $comments,
	    );
	    
	}
	$output = array(
	    "status" => "ok", 
	    "posts" => $posts
	);
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
    public function actionFeedForUser() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	
	$input = file_get_contents('php://input');
	
	$u = json_decode($input,true);
	$user_id = $u["user_id"];
	$viewer_id = $u["viewer_id"];
	$user = $this->users->getUser($user_id);
	$viewer = $this->users->getUser($viewer_id);
	if (!$user || !$viewer) {
	    $output = array("status" => "error", "message" => "User does not exist.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	
	$posts = array();
	$viewer_mode = Model\Users::NON_FRIEND;
	if($this->friends->getFriendState($user_id, $viewer_id) == \Model\Friends::ACCEPTED) {
	    $viewer_mode = Model\Users::FRIEND;
	}
	$feed = $this->users->getUsersWall($user_id, $viewer_mode, $viewer_id);
	
	foreach($feed as $post) {
	    $comments = array();
	    foreach($post->related('comments')->order('date') as $comment) {
		$comments[] = array(
		    'id' => $comment->id,
		    'author' => $comment->user_id,
		    'date' => $comment->date->format('c'),
		    'text' => $comment->content
		);
	    }
	    $posts[] = array(
		'id' => $post->id,
		'author' => $post->user_id,
		'date' => $post->date->format('c'),
		'text' => $post->content,
		'comments' => $comments,
	    );
	    
	}
	$output = array(
	    "status" => "ok", 
	    "posts" => $posts
	);
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
    public function actionAddComment() {
	header('content-type: application/json; charset=utf-8');
	header("access-control-allow-origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	
	$input = file_get_contents('php://input');
	
	$u = json_decode($input,true);
	$user_id = $u["user_id"];
	$post_id = $u["post_id"];
	$text = $u['text'];
	
	$user = $this->users->getUser($user_id);
	if (!$user) {
	    $output = array("status" => "error", "message" => "User does not exist.");
	    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
	}
	
	$this->users->addComment($user_id, $post_id, $text);
	$output = array(
	    "status" => "ok",
	);
	$this->sendResponse(new \Nette\Application\Responses\JsonResponse($output));
    }
    
}

?>
