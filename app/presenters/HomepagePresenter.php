<?php

/**
 *
 * @author Tomáš Skalický
 */
class HomepagePresenter extends BasePresenter
{
	/** @var Model\Users @inject*/
	public $users;
	
	public function renderDefault()
	{
		$this->template->userslist = $this->users->findAll();
	}
}
