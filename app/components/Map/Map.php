<?php
namespace Components;
/**
 * Map component
 *
 * @author Skalda
 */
class Map extends \Nette\Application\UI\Control {

    static $mapurl = "http://maps.googleapis.com/maps/api/staticmap?size={size}&sensor=false&path={path}";
    private $kml = "";
    private $width = 800;
    private $height = 450;
    
    public function setKML($kml) {
	$this->kml = $kml;
    }
    
    public function setSize($width, $height) {
	$this->width = $width;
	$this->height = $height;
    }
    
    
    
    
    /**
     * (non-phpDoc)
     *
     * @see Nette\Application\Control#render()
     */
    public function render() {
	$this->template->kml = $this->kml . '?t='.  round(time());
	$this->template->width = $this->width;
	$this->template->height = $this->height;
	$this->template->setFile(dirname(__FILE__) . '/map.latte');
	$this->template->render();
    }
}