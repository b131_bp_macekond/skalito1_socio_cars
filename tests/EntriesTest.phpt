<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class EntriesTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\Entries */
	private $model;
 
	
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\Entries');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('entries')->delete();
		$this->database->table('route_users')->delete();
		$this->database->table('routes')->delete();
		$this->database->table('vehicle_drivers')->delete();
		$this->database->table('vehicle_rent')->delete();
		$this->database->table('vehicles')->delete();
		$this->database->table('users')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		
		$this->database->table('vehicles')->insert(array(
		    'id' => 1,
		    'users_id' => 1,
		    'unit_id' => 1234,
		    'active' => 1,
		    'name' => 'Auto1',
		    'profilpic' => 'auto1.jpg',
		    'desc' => 'popis auta',
		    'registration_number' => '132asd6',
		    'type' => 1,
		    'vehicle_make' => 5,
		    'vehicle_type' => '2.0',
		    'commercial_description' => 'mondeo',
		    'vin' => 'aasdzxc13265498',
		    'mass_permissible' => '2000',
		    'mass_operating' => '1500',
		    'engine_capacity' => '1988',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'BA 95 B',
		    'seating' => '5',
		    'stands' => '0',
		    'color' => 'červena-metal',
		    'power_weigth' => null,
		    'maximum_speed' => '199',
		    'mileage' => 0,
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 1,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '01:57:30',
		    'length' => '94.04',
		    'start_lat' => '49.73',
		    'start_long' => '13.37',
		    'end_lat' => '50.07',
		    'end_long' => '14.43',
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 2,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Zbečno - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '03:42:45',
		    'length' => 115.16,
		    'start_lat' => 49.73,
		    'start_long' => 13.37,
		    'end_lat' => 50.07,
		    'end_long' => 14.43,
		));
		
		$this->database->table('entries')->insert(array(
		    'id' => 1,
		    'routes_id' => 1,
		    'longitude' => 49.73,
		    'latitude' => 13.37,
		    'timestamp' => '2014-04-20 15:16:32',
		    'odometer' => 12,
		    'velocity' => 15,
		    'consumption' => 10.2,
		    'fuel_remaining' => 12.7,
		    'altitude' => 0,
		    'engine_temp' => 25,
		    'engine_rpm' => 1600,
		    'throttle' => 12,
		));
		$this->database->table('entries')->insert(array(
		    'id' => 2,
		    'routes_id' => 1,
		    'longitude' => 50.07,
		    'latitude' => 14.43,
		    'timestamp' => '2014-04-20 15:17:02',
		    'odometer' => 15,
		    'velocity' => 16,
		    'consumption' => 12.2,
		    'fuel_remaining' => 10.8,
		    'altitude' => 10,
		    'engine_temp' => 50,
		    'engine_rpm' => 1900,
		    'throttle' => 13,
		));
		
		$this->database->table('entries')->insert(array(
		    'id' => 3,
		    'routes_id' => 2,
		    'longitude' => 49.73,
		    'latitude' => 13.37,
		    'timestamp' => '2014-04-20 15:16:32',
		    'odometer' => 12,
		    'velocity' => 15,
		    'consumption' => 10.2,
		    'fuel_remaining' => 12.7,
		    'altitude' => 0,
		    'engine_temp' => 25,
		    'engine_rpm' => 1600,
		    'throttle' => 12,
		));
		$this->database->table('entries')->insert(array(
		    'id' => 4,
		    'routes_id' => 2,
		    'longitude' => 49.97,
		    'latitude' => 14.00,
		    'timestamp' => '2014-04-20 15:17:02',
		    'odometer' => 15,
		    'velocity' => 16,
		    'consumption' => 12.2,
		    'fuel_remaining' => 10.8,
		    'altitude' => 10,
		    'engine_temp' => 50,
		    'engine_rpm' => 1900,
		    'throttle' => 13,
		));
		$this->database->table('entries')->insert(array(
		    'id' => 5,
		    'routes_id' => 2,
		    'longitude' => 50.07,
		    'latitude' => 14.43,
		    'timestamp' => '2014-04-20 15:17:32',
		    'odometer' => 15,
		    'velocity' => 16,
		    'consumption' => 12.2,
		    'fuel_remaining' => 10.8,
		    'altitude' => 10,
		    'engine_temp' => 50,
		    'engine_rpm' => 1900,
		    'throttle' => 13,
		));
	}
 
	function testAddEntry() {
	    $routeId = 1;
	    $location = array(49.79, 13.97);
	    $timestamp = "2012-10-24T01:00:27.372Z";
	    $odometer = 2.0;
	    $velocity = 5.0;
	    $consumption = 12.5;
	    $fuel_remaining = 10.0;
	    $altitude = 8.0;
	    $engine_temp = 30.0;
	    $engine_rpm = 2200.0;
	    $throttle = 15.0;
	    $return = $this->model->addEntry($routeId, $location, $timestamp, $odometer, $velocity, $consumption, $fuel_remaining, $altitude, $engine_temp, $engine_rpm, $throttle);
	    Assert::equal($this->database->table('entries')->count(), 6);
	    $row = $this->database->table('entries')->get($return->id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal($routeId, $row->routes_id);
	    Assert::equal($location[0], $row->longitude);
	    Assert::equal($location[1], $row->latitude);
	    Assert::equal($odometer, $row->odometer);
	    Assert::equal($velocity, $row->velocity);
	    Assert::equal($consumption, $row->consumption);
	    Assert::equal($fuel_remaining, $row->fuel_remaining);
	    Assert::equal($altitude, $row->altitude);
	    Assert::equal($engine_temp, $row->engine_temp);
	    Assert::equal($engine_rpm, $row->engine_rpm);
	    Assert::equal($throttle, $row->throttle);
	}
	
	function testGetRoutesEntries() {
	    $routeId = 2;
	    $return = $this->model->getRoutesEntries($routeId);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(3, $return->count());
	}
	
	function testGetEntry() {
	    $id = 2;
	    $row = $this->model->getEntry($id);
	    $route = array('id' => 2,
		    'routes_id' => 1,
		    'longitude' => 50.07,
		    'latitude' => 14.43,
		    'odometer' => 15.0,
		    'velocity' => 16.0,
		    'consumption' => 12.2,
		    'fuel_remaining' => 10.8,
		    'altitude' => 10.0,
		    'engine_temp' => 50.0,
		    'engine_rpm' => 1900.0,
		    'throttle' => 13.0,);
	    Assert::equal($route['routes_id'], $row['routes_id']);
	    Assert::equal($route['longitude'], $row['longitude']);
	    Assert::equal($route['latitude'], $row['latitude']);
	    Assert::equal($route['odometer'], $row['odometer']);
	    Assert::equal($route['velocity'], $row['velocity']);
	    Assert::equal($route['consumption'], $row['consumption']);
	    Assert::equal($route['fuel_remaining'], $row['fuel_remaining']);
	    Assert::equal($route['altitude'], $row['altitude']);
	    Assert::equal($route['engine_temp'], $row['engine_temp']);
	    Assert::equal($route['engine_rpm'], $row['engine_rpm']);
	    Assert::equal($route['throttle'], $row['throttle']);
	}
	
	function testGetEntry_notExists() {
	    $id = 10;
	    $row = $this->model->getEntry($id);
	    
	    Assert::null($row);
	}
	

}


id(new EntriesTest($container))->run();