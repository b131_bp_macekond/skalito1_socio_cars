<?php
 
require __DIR__ . '/../libs/autoload.php';
define('TEMP_DIR', __DIR__.'/temp'); 
define('WWW_DIR', __DIR__); 
if (!class_exists('Tester\Assert')) {
	echo "Install Nette Tester using `composer update --dev`\n";
	exit(1);
}
 
Tester\Environment::setup();
 
function id($val) {
	return $val;
}
 
$configurator = new Nette\Configurator;
$configurator->setDebugMode(FALSE);
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
	->addDirectory(__DIR__ . '/../app')
	->addDirectory(__DIR__ . '/../libs')
	->register();
 
$configurator->addConfig(__DIR__ . '/../app/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.test.neon');
return $configurator->createContainer();