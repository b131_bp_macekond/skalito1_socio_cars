<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class FriendsTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\Friends */
	private $model;
 
	
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\Friends');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
		
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('users')->delete();
		$this->database->table('notifications')->delete();
		$this->database->table('friends')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 3,
		    'email' => 'user3@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf3',
		    'salt' => 'qwertqwertqwertqwer3',
		    'name' => 'User3',
		    'surname' => 'Usersky3',
		    'sex' => 'female',
		    'city' => 'Prague3',
		    'profilpic' => 'obrazek3.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 4,
		    'email' => 'user4@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf4',
		    'salt' => 'qwertqwertqwertqwer4',
		    'name' => 'User4',
		    'surname' => 'Usersky4',
		    'sex' => 'female',
		    'city' => 'Prague4',
		    'profilpic' => 'obrazek4.jpg',
		));
		
		$this->database->table('friends')->insert(array(
		    'from' => 1,
		    'to' => 3,
		    'accepted' => 1,
		));
		$this->database->table('friends')->insert(array(
		    'from' => 4,
		    'to' => 1,
		    'accepted' => 1,
		));
		$this->database->table('friends')->insert(array(
		    'from' => 2,
		    'to' => 3,
		    'accepted' => 0,
		));
		$this->database->table('friends')->insert(array(
		    'from' => 2,
		    'to' => 4,
		    'accepted' => 1,
		));
	}
 
	
	function testGetFriendState_NONE() {
		$state = $this->model->getFriendState(1, 2);
		Assert::equal(\Model\Friends::NONE, $state);
		$state = $this->model->getFriendState(2, 1);
		Assert::equal(\Model\Friends::NONE, $state);
	}
	
	function testGetFriendState_ACCEPTED() {
		$state = $this->model->getFriendState(1, 3);
		Assert::equal(\Model\Friends::ACCEPTED, $state);
		$state = $this->model->getFriendState(3, 1);
		Assert::equal(\Model\Friends::ACCEPTED, $state);
	}
	
	function testGetFriendState_PENDINGFROM() {
		$state = $this->model->getFriendState(2, 3);
		Assert::equal(\Model\Friends::PENDING_FROM, $state);
	}
	function testGetFriendState_PENDINGTO() {
		$state = $this->model->getFriendState(3, 2);
		Assert::equal(\Model\Friends::PENDING_TO, $state);
	}
	
	function testAddingFriendsProcess()
	{
		$state = $this->model->getFriendState(1, 2);
		Assert::equal(\Model\Friends::NONE, $state);
		$this->model->addFriend(1, 2);
		$model = $this->model;
		Assert::exception(function() use ($model) {
		    $this->model->addFriend(1, 2);
		}, 'Model\FriendRequestAlreadyPendingException');
		$this->model->acceptFriend(2, 1);
		$state = $this->model->getFriendState(1, 2);
		Assert::equal(\Model\Friends::ACCEPTED, $state);
		$notification1 = $this->database->table('notifications')->where('users_id', 1);
		Assert::equal(1, $notification1->count());
		$row = $notification1->fetch();
		Assert::equal($row->link, 'User:');
		Assert::equal($row->message, 'Uživatel {person} potvrdil vaší žádost o přátelství');
		Assert::equal($row->link_params, json_encode(array('id' => 2)));
		$notification2 = $this->database->table('notifications')->where('users_id', 2);
		Assert::equal(1, $notification2->count());
		$row = $notification2->fetch();
		Assert::equal($row->link, 'User:');
		Assert::equal($row->message, 'Uživatel {person} váš požádat o přátelství');
		Assert::equal($row->link_params, json_encode(array('id' => 1)));
	}
	
	function testGetUsersFriends() {
		$row = $this->model->getUsersFriends(1);
		Assert::true($row instanceof \Nette\Database\Table\Selection);
		Assert::equal(2, $row->count());
	}
	
	function testGetUsersFriends_notAllAccepted() {
		$row = $this->model->getUsersFriends(2);
		Assert::true($row instanceof \Nette\Database\Table\Selection);
		Assert::equal(1, $row->count());
	}
	
	function testRemoveFriend() {
		$this->model->removeFriend(1, 3);
		$row = $this->database->table('friends')->where(array(
		    'from' => 1,
		    'to' => 3,
		));
		Assert::equal(0, $row->count());
	}
	
	function testRemoveFriend_notExistingFriendship() {
		$model = $this->model;
		Assert::exception(function() use ($model){
		    $model->removeFriend(1, 2);
		}, 'Model\FriendRequestDoesNotExistException');
	}
	
	function testAcceptFriend() {
	    $this->model->acceptFriend(3, 2);
	    $row = $this->database->table('friends')->where(array(
		    'from' => 2,
		    'to' => 3,
		))->fetch();
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1, $row->accepted);
	}
	
	function testAcceptFriend_WrongOrder() {
	    
	    $model = $this->model;
		Assert::exception(function() use ($model){
		    $this->model->acceptFriend(2, 3);
		}, 'Model\FriendRequestDoesNotExistException');
	}
	
	function testAcceptFriend_NotPending() {
	    
	    $model = $this->model;
		Assert::exception(function() use ($model){
		    $this->model->acceptFriend(1, 2);
		}, 'Model\FriendRequestDoesNotExistException');
	}

}


id(new FriendsTest($container))->run();