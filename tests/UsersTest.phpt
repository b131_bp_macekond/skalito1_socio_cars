<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class UsersTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\Users */
	private $model;
 
	
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\Users');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
		
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('comments')->delete();
		$this->database->table('wall')->delete();
		$this->database->table('users')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'a0bf8f4612891f3b3865f0ee69831a9ba873076d', //qwerty123
		    'salt' => '321ytrewq',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 3,
		    'email' => 'user3@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf3',
		    'salt' => 'qwertqwertqwertqwer3',
		    'name' => 'User3',
		    'surname' => 'Usersky3',
		    'sex' => 'female',
		    'city' => 'Prague3',
		    'profilpic' => 'obrazek3.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 4,
		    'email' => 'user4@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf4',
		    'salt' => 'qwertqwertqwertqwer4',
		    'name' => 'User4',
		    'surname' => 'Usersky4',
		    'sex' => 'female',
		    'city' => 'Prague4',
		    'profilpic' => 'obrazek4.jpg',
		));
		
		$this->database->table('friends')->insert(array(
		    'id' => 1,
		    'from' => 1,
		    'to' => 3,
		    'accepted' => 1,
		));
		$this->database->table('friends')->insert(array(
		    'id' => 2,
		    'from' => 4,
		    'to' => 1,
		    'accepted' => 1,
		));
		$this->database->table('friends')->insert(array(
		    'id' => 3,
		    'from' => 2,
		    'to' => 3,
		    'accepted' => 0,
		));
		$this->database->table('friends')->insert(array(
		    'id' => 4,
		    'from' => 2,
		    'to' => 4,
		    'accepted' => 1,
		));
		
		$this->database->table('friends')->insert(array(
		    'id' => 5,
		    'from' => 1,
		    'to' => 2,
		    'accepted' => 1,
		));
		
		$this->database->table('friends_circles')->insert(array(
		    'id' => 1,
		    'user_id' => 1,
		    'name' => 'Kruh 1',
		));
		
		$this->database->table('friends_circles')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'name' => 'Kruh 2',
		));
		
		$this->database->table('friends_circles')->insert(array(
		    'id' => 3,
		    'user_id' => 3,
		    'name' => 'Kruh 3',
		));
		$this->database->table('friends_to_circles')->insert(array(
		    'id' => 1,
		    'friend_id' => 1,
		    'users_id' => 3,
		    'friends_circle_id' => 1
		));
		$this->database->table('friends_to_circles')->insert(array(
		    'id' => 2,
		    'friend_id' => 1,
		    'users_id' => 1,
		    'friends_circle_id' => 3
		));
		$this->database->table('friends_to_circles')->insert(array(
		    'id' => 3,
		    'friend_id' => 2,
		    'users_id' => 4,
		    'friends_circle_id' => 1
		));
		
		$this->database->table('wall')->insert(array(
		    'id' => 1,
		    'user_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 1',
		    'privacy' => 0,
		));
		$this->database->table('wall')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 1',
		    'privacy' => 2,
		));
		
		$this->database->table('wall_to_circles')->insert(array(
		    'wall_id' => 2,
		    'friends_circle_id' => 1,
		));
		
		$this->database->table('wall')->insert(array(
		    'id' => 3,
		    'user_id' => 2,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 2',
		    'privacy' => 0,
		));
		$this->database->table('wall')->insert(array(
		    'id' => 4,
		    'user_id' => 4,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 3',
		    'privacy' => 1,
		));
		$this->database->table('wall')->insert(array(
		    'id' => 5,
		    'user_id' => 2,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 4',
		    'privacy' => 1,
		));
		$this->database->table('wall')->insert(array(
		    'id' => 6,
		    'user_id' => 4,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 5',
		    'privacy' => 0,
		));
		
		$this->database->table('comments')->insert(array(
		    'id' => 1,
		    'user_id' => 2,
		    'wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 1',
		));
		$this->database->table('comments')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 2',
		));
		$this->database->table('comments')->insert(array(
		    'id' => 3,
		    'user_id' => 4,
		    'wall_id' => 5,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 3',
		));
		$this->database->table('comments')->insert(array(
		    'id' => 4,
		    'user_id' => 2,
		    'wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 4',
		));
		
		$this->database->table('groups')->insert(array(
		    'id' => 1,
		    'user_id' => 1,
		    'name' => 'Group1',
		    'desc' => 'description1',
		));
		
		$this->database->table('groups')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'name' => 'Group2',
		    'desc' => 'description2',
		));
		
		$this->database->table('groups')->insert(array(
		    'id' => 3,
		    'user_id' => 4,
		    'name' => 'Group3',
		    'desc' => 'description3',
		));
		
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 1,
		    'user_id' => 1,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 1,
		    'user_id' => 2,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 1,
		    'user_id' => 4,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 2,
		    'user_id' => 2,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 2,
		    'user_id' => 1,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 3,
		    'user_id' => 4,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 3,
		    'user_id' => 2,
		));
		
		$this->database->table('groups_wall')->insert(array(
		    'id' => 1,
		    'group_id' => 1,
		    'user_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 1',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 2,
		    'group_id' => 1,
		    'user_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 1',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 3,
		    'group_id' => 1,
		    'user_id' => 2,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 2',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 4,
		    'group_id' => 3,
		    'user_id' => 4,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 3',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 5,
		    'group_id' => 3,
		    'user_id' => 2,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 4',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 6,
		    'group_id' => 1,
		    'user_id' => 4,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 5',
		));
		
		$this->database->table('groups_comments')->insert(array(
		    'id' => 1,
		    'user_id' => 2,
		    'groups_wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 1',
		));
		$this->database->table('groups_comments')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'groups_wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 2',
		));
		$this->database->table('groups_comments')->insert(array(
		    'id' => 3,
		    'user_id' => 4,
		    'groups_wall_id' => 5,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 3',
		));
		$this->database->table('groups_comments')->insert(array(
		    'id' => 4,
		    'user_id' => 2,
		    'groups_wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 4',
		));
	}
	
	function testGetPasswordHash() {
	    $password = "qwerty123";
	    $salt = "321ytrewq";
	    $return = $this->model->getPasswordHash($password, $salt);
	    Assert::equal(sha1($password . "are14%!u@#raia" . $salt), $return);
	}
	
	function testAddUser() {
	    $email = "test@test.test";
	    $password = "test";
	    $return = $this->model->addUser($email, $password);
	    Assert::equal(5, $this->database->table('users')->count());
	    $row = $this->database->table('users')->wherePrimary($return->id)->fetch();
	    Assert::equal($email, $row->email);
	}
	
	function testAddUser_alreadyExists() {
	    $model = $this->model;
	    Assert::exception(function() use ($model) {
		$email = "user4@user.cz";
		$password = "test";
		$this->model->addUser($email, $password);
	    }, 'Model\DuplicateEntryException');
	}
	
	function testChangePassword() {
	    $id = 1;
	    $oldPassword = 'qwerty123';
	    $newPassword = 'asdfgh123';
	    $return = $this->model->changePassword($id, $oldPassword, $newPassword);
	    $row = $this->database->table('users')->wherePrimary($id)->fetch();
	    Assert::equal(sha1($newPassword . "are14%!u@#raia" . $row->salt), $row->password);
	}
	
	function testChangePassword_wrongOldPassword() {
	    
	    $model = $this->model;
	    Assert::exception(function() use ($model) {
		$id = 1;
		$oldPassword = 'aqwerty123';
		$newPassword = 'asdfgh123';
		$return = $this->model->changePassword($id, $oldPassword, $newPassword);
	    }, 'Model\WrongPasswordException');
	}
	
	function testModifyUser() {
	    $id = 1;
	    $name = "Změněné jméno";
	    $surname = "Změněné přijmení";
	    $sex = "female";
	    $city = "Změněné heslo";
	    $return = $this->model->modifyUser($id, $name, $surname, $sex, $city);
	    Assert::equal(4, $this->database->table('users')->count());
	    $row = $this->database->table('users')->wherePrimary($id)->fetch();
	    Assert::equal($name, $row->name);
	    Assert::equal($surname, $row->surname);
	    Assert::equal($sex, $row->sex);
	    Assert::equal($city, $row->city);
	    Assert::equal(1, $row->filled_data);
	}
	
	function testChangeProfilePic() {
	    $id = 1;
	    $pic = "nová fotka";
	    $this->model->changeProfilePic($id, $pic);
	    Assert::equal(4, $this->database->table('users')->count());
	    $row = $this->database->table('users')->wherePrimary($id)->fetch();
	    Assert::equal($pic, $row->profilpic);
	}
	
	function testGetUser() {
	    $id = 1;
	    $row = $this->model->getUser($id);
	    Assert::equal('user@user.cz', $row['email']);
	    Assert::equal('User', $row['name']);
	    Assert::equal('Usersky', $row['surname']);
	    Assert::equal('male', $row['sex']);
	    Assert::equal('Prague', $row['city']);
	    Assert::equal('obrazek.jpg', $row['profilpic']);
	}
	
	function testGetUser_notExists() {
	    $id = 10;
	    $row = $this->model->getUser($id);
	    Assert::null($row);
	}
	
	function testGetUserByEmailAndPassword() {
	    $email = "user@user.cz";
	    $password = "qwerty123";
	    $row = $this->model->getUserByEmailAndPassword($email, $password);
	    Assert::equal(1, $row['id']);
	    Assert::equal('user@user.cz', $row['email']);
	    Assert::equal('User', $row['name']);
	    Assert::equal('Usersky', $row['surname']);
	    Assert::equal('male', $row['sex']);
	    Assert::equal('Prague', $row['city']);
	    Assert::equal('obrazek.jpg', $row['profilpic']);
	}
	
	function testGetUserByEmailAndPassword_notExistEmail() {
	    $email = "email@neexistuje.cz";
	    $password = "qwerty123";
	    $row = $this->model->getUserByEmailAndPassword($email, $password);
	    Assert::false($row);
	}
	function testGetUserByEmailAndPassword_notExistPassword() {
	    $email = "user@user.cz";
	    $password = "asdjlk";
	    $row = $this->model->getUserByEmailAndPassword($email, $password);
	    Assert::false($row);
	}
	
	function testVerifyUsersPassword() {
	    $id = 1;
	    $password = "qwerty123";
	    Assert::true($this->model->verifyUsersPassword($id, $password));
	}
	
	function testVerifyUsersPassword_notExists() {
	    $id = 10;
	    $password = "qwerty123";
	    Assert::false($this->model->verifyUsersPassword($id, $password));
	}
	
	function testSearchUser_byName() {
	    $q = "user2";
	    $res = $this->model->searchUser($q);
	    Assert::equal(1, $res->count());
	    Assert::equal("User2", $res->fetch()->name);
	}
	
	function testSearchUser_bySurname() {
	    $q = "usersky2";
	    $res = $this->model->searchUser($q);
	    Assert::equal(1, $res->count());
	    Assert::equal("Usersky2", $res->fetch()->surname);
	}
	
	function testSearchUser_byEmail() {
	    $q = "user2@user";
	    $res = $this->model->searchUser($q);
	    Assert::equal(1, $res->count());
	    Assert::equal("user2@user.cz", $res->fetch()->email);
	}
	
	function testGetUsersWall_author() {
	    $id = 1;
	    $return = $this->model->getUsersWall($id, \Model\Users::AUTHOR);
	    Assert::equal(2, count($return));
	}
	
	function testGetUsersWall_nonFriend() {
	    $id = 1;
	    $return = $this->model->getUsersWall($id);
	    Assert::equal(1, count($return));
	}
	
	function testGetUsersWall_friend() {
	    $id = 1;
	    $return = $this->model->getUsersWall($id, \Model\Users::FRIEND, 3);
	    Assert::equal(2, count($return));
	}
	
	function testGetUsersWall_friend2() {
	    $id = 1;
	    $return = $this->model->getUsersWall($id, \Model\Users::FRIEND, 2);
	    Assert::equal(1, count($return));
	}
	
	function testGetFriendsWallPosts() {
	    $user_id = 1;
	    $friends = array(3, 4);
	    $return = $this->model->getFriendsWallPosts($friends, $user_id);
	    Assert::equal(2, count($return));
	}
	    
	function testGetGroupPosts() {
	    $groups = array(1, 2);
	    $return = $this->model->getGroupPosts($groups);
	    Assert::equal(4, count($return));
	}
	    
	function testGetFeedPosts() {
	    $user_id = 1;
	    $friends = array(3, 4);
	    $groups = array(1, 2);
	    $return = $this->model->getFeedPosts($friends, $groups, $user_id);
	    Assert::equal(6, count($return));
	}
	
	function testGetWallPostCircles() {
	    $wall_id = 2;
	    $return = $this->model->getWallPostCircles($wall_id);
	    Assert::equal(1, count($return));
	    Assert::equal('Kruh 1', $return->fetch()->ref('friends_circle')->name);
	}
	
	function testGetCirclesUsersIsIn() {
	    $user_id = 1;
	    $friend_id = 3;
	    $return = $this->model->getCirclesUsersIsIn($user_id, $friend_id);
	    Assert::equal(1, count($return));
	}
	
	function testGetSingleWallPost() {
	    $id = 1;
	    $return = $this->model->getSingleWallPost($id);
	    Assert::equal(1, count($return));
	    Assert::equal('Vzkaz 1', $return->fetch()->content);
	}
	
	function testGetSingleWallPost2() {
	    $id = 4;
	    $return = $this->model->getSingleWallPost($id, \Model\Users::FRIEND);
	    Assert::equal(1, count($return));
	    Assert::equal('Vzkaz 3', $return->fetch()->content);
	}
	
	function testGetSingleWallPost3() {
	    $id = 4;
	    $return = $this->model->getSingleWallPost($id);
	    Assert::equal(0, count($return));
	}
	
	function testAddWallPost() {
	    $id = 1;
	    $content = "Vzkaz";
	    $return = $this->model->addWallPost($id, $content);
	    Assert::equal(7, $this->database->table('wall')->count());
	    $row = $this->database->table('wall')->wherePrimary($return->id)->fetch();
	    Assert::equal($content, $row->content);
	    Assert::equal(\Model\Users::ALL, $row->privacy);
	}
	
	function testAddWallPost2() {
	    $id = 1;
	    $content = "Vzkaz";
	    $return = $this->model->addWallPost($id, $content, \Model\Users::FRIENDS_ONLY);
	    Assert::equal(7, $this->database->table('wall')->count());
	    $row = $this->database->table('wall')->wherePrimary($return->id)->fetch();
	    Assert::equal($content, $row->content);
	    Assert::equal(\Model\Users::FRIENDS_ONLY, $row->privacy);
	}
	
	function testAddWallPost3() {
	    $id = 1;
	    $content = "Vzkaz";
	    $circles = array(1, 2);
	    $return = $this->model->addWallPost($id, $content, \Model\Users::CIRCLES, null, $circles);
	    Assert::equal(7, $this->database->table('wall')->count());
	    $row = $this->database->table('wall')->wherePrimary($return->id)->fetch();
	    Assert::equal($content, $row->content);
	    Assert::equal(\Model\Users::CIRCLES, $row->privacy);
	    Assert::equal(3, $this->database->table('wall_to_circles')->count());
	}
	
	function testAddComment() {
	    $wall_id = 3;
	    $user_id = 1;
	    $content = "Nový komentář";
	    $result = $this->model->addComment($user_id, $wall_id, $content);
	    Assert::equal(5, $this->database->table('comments')->count());
	    $row = $this->database->table('comments')->get($result->id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal($user_id, $row->user_id);
	    Assert::equal($wall_id, $row->wall_id);
	    Assert::equal($content, $row->content);
	}
	
	function testAunthenticate_ok() {
	    $creadentials = array('user@user.cz', 'qwerty123');
	    $return = $this->model->authenticate($creadentials);
	    Assert::true($return instanceof \Nette\Security\Identity);
	}
	
	function testAunthenticate_wrongEmail() {
	    $model = $this->model;
	    Assert::exception(function() use ($model) {
		$creadentials = array('user1@user.cz', 'qwerty123');
		$return = $model->authenticate($creadentials);
	    }, 'Nette\Security\AuthenticationException');
	}
	
	function testAunthenticate_wrongPassword() {
	    $model = $this->model;
	    Assert::exception(function() use ($model) {
		$creadentials = array('user@user.cz', 'qwerty123a');
		$return = $model->authenticate($creadentials);
	    }, 'Nette\Security\AuthenticationException');
	}
}


id(new UsersTest($container))->run();