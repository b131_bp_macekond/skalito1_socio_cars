<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class RouteUsersTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\RouteUsers */
	private $model;
 
	
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\RouteUsers');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
		
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('entries')->delete();
		$this->database->table('route_users')->delete();
		$this->database->table('routes')->delete();
		$this->database->table('vehicle_drivers')->delete();
		$this->database->table('vehicle_rent')->delete();
		$this->database->table('vehicles')->delete();
		$this->database->table('users')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		
		$this->database->table('vehicles')->insert(array(
		    'id' => 1,
		    'users_id' => 1,
		    'unit_id' => 1234,
		    'active' => 1,
		    'name' => 'Auto1',
		    'profilpic' => 'auto1.jpg',
		    'desc' => 'popis auta',
		    'registration_number' => '132asd6',
		    'type' => 1,
		    'vehicle_make' => 5,
		    'vehicle_type' => '2.0',
		    'commercial_description' => 'mondeo',
		    'vin' => 'aasdzxc13265498',
		    'mass_permissible' => '2000',
		    'mass_operating' => '1500',
		    'engine_capacity' => '1988',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'BA 95 B',
		    'seating' => '5',
		    'stands' => '0',
		    'color' => 'červena-metal',
		    'power_weigth' => null,
		    'maximum_speed' => '199',
		    'mileage' => 0,
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 1,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '01:57:30',
		    'length' => '94.04',
		    'start_lat' => '49.73',
		    'start_long' => '13.37',
		    'end_lat' => '50.07',
		    'end_long' => '14.43',
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 2,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Zbečno - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '03:42:45',
		    'length' => 115.16,
		    'start_lat' => 49.73,
		    'start_long' => 13.37,
		    'end_lat' => 50.07,
		    'end_long' => 14.43,
		));
		
		$this->database->table('route_users')->insert(array(
		    'routes_id' => 1,
		    'users_id' => 2,
		));
	}
 
	function testGetRoutesUsers() {
	    $return = $this->model->getRoutesUsers(1);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $return->count());
	    Assert::equal(2, $return->fetch()->id);
	}

}


id(new RouteUsersTest($container))->run();