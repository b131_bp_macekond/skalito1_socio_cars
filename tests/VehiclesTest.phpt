<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class VehiclesTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\Vehicles */
	private $model;
 
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\Vehicles');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
	    
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('users')->delete();
		$this->database->table('vehicles')->delete();
		$this->database->table('vehicle_rent')->delete();
		$this->database->table('vehicle_type')->delete();
		$this->database->table('vehicle_make')->delete();
		$this->database->table('vehicle_drivers')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 3,
		    'email' => 'user3@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf3',
		    'salt' => 'qwertqwertqwertqwer3',
		    'name' => 'User3',
		    'surname' => 'Usersky3',
		    'sex' => 'female',
		    'city' => 'Prague3',
		    'profilpic' => 'obrazek3.jpg',
		));
		
		$this->database->query("INSERT INTO `vehicle_type` (`id`, `name`) VALUES
			(1,	'Osobní'),
			(2,	'Terénní'),
			(3,	'Užitkové'),
			(4,	'Nákladní'),
			(5,	'Autobus'),
			(6,	'Obytné'),
			(7,	'Přívěs'),
			(8,	'Motorka'),
			(9,	'Ostatní'),
			(10,	'Pracovní stroj');");
		
		$this->database->query("INSERT INTO `vehicle_make` (`id`, `name`) VALUES
			(1,	'Abarth'),
			(2,	'AC'),
			(3,	'Acura'),
			(4,	'Aixam'),
			(5,	'Alfa Romeo'),
			(6,	'Alpina'),
			(7,	'AM General Truck'),
			(8,	'ARO'),
			(9,	'Asia Motors'),
			(10,	'Aston Martin'),
			(11,	'Audi'),
			(12,	'Austin'),
			(13,	'Auverland'),
			(14,	'Bellier'),
			(15,	'Bentley');");
		
		$this->database->table('vehicles')->insert(array(
		    'id' => 1,
		    'users_id' => 1,
		    'unit_id' => 1234,
		    'active' => 1,
		    'name' => 'Auto1',
		    'profilpic' => 'auto1.jpg',
		    'desc' => 'popis auta',
		    'registration_number' => '132asd6',
		    'type' => 1,
		    'vehicle_make' => 5,
		    'vehicle_type' => '2.0',
		    'commercial_description' => 'mondeo',
		    'vin' => 'aasdzxc13265498',
		    'mass_permissible' => '2000',
		    'mass_operating' => '1500',
		    'engine_capacity' => '1988',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'BA 95 B',
		    'seating' => '5',
		    'stands' => '0',
		    'color' => 'červena-metal',
		    'power_weigth' => null,
		    'maximum_speed' => '199',
		    'mileage' => 0,
		));
		$this->database->table('vehicles')->insert(array(
		    'id' => 2,
		    'users_id' => 1,
		    'unit_id' => 1200,
		    'active' => 0,
		    'name' => 'Auto2',
		    'profilpic' => 'auto2.jpg',
		    'desc' => 'popis auta2',
		    'registration_number' => '321asd6',
		    'type' => 2,
		    'vehicle_make' => 10,
		    'vehicle_type' => '1',
		    'commercial_description' => 'moto',
		    'vin' => 'aasdzxc13265400',
		    'mass_permissible' => '1000',
		    'mass_operating' => '500',
		    'engine_capacity' => '1000',
		    'engine_max_net_power' => '500',
		    'engine_fuel' => 'BA 95 B',
		    'seating' => '2',
		    'stands' => '0',
		    'color' => 'černá',
		    'power_weigth' => '12',
		    'maximum_speed' => '250',
		    'mileage' => 40,
		));
		$this->database->table('vehicles')->insert(array(
		    'id' => 3,
		    'users_id' => 2,
		    'unit_id' => 1100,
		    'active' => 1,
		    'name' => 'Auto3',
		    'profilpic' => 'auto3.jpg',
		    'desc' => 'popis auta3',
		    'registration_number' => '132as30',
		    'type' => 1,
		    'vehicle_make' => 15,
		    'vehicle_type' => 'typ',
		    'commercial_description' => 'obchod',
		    'vin' => 'aasdzxc13260000',
		    'mass_permissible' => '2500',
		    'mass_operating' => '2000',
		    'engine_capacity' => '2500',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'benzin',
		    'seating' => '6',
		    'stands' => '2',
		    'color' => 'žlutá',
		    'power_weigth' => null,
		    'maximum_speed' => '120',
		    'mileage' => 0,
		));
		
		$this->database->table('vehicle_drivers')->insert(array(
		    'id' => 1,
		    'vehicle_id' => 1,
		    'user_id' => 2,
		));
		
		$this->database->table('vehicle_drivers')->insert(array(
		    'id' => 2,
		    'vehicle_id' => 3,
		    'user_id' => 1,
		));
		
		$this->database->table('vehicle_rent')->insert(array(
		    'user_id' => 1,
		    'vehicle_id' => 2,
		    'from' => '2014-04-19',
		    'to' => '2014-04-20',
		));
		
		
	}
 
	function testAddVehicle()
	{
		$info = array(
		    'unit_id' => 105,
		    'name' => 'Auto4',
		    'desc' => 'popis auta4',
		    'registration_number' => '132as40',
		    'type' => 1,
		    'vehicle_make' => 5,
		    'vehicle_type' => 'typ',
		    'commercial_description' => 'obchod',
		    'vin' => 'aasdzxc13260004',
		    'mass_permissible' => '2400',
		    'mass_operating' => '2004',
		    'engine_capacity' => '2504',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'nafta',
		    'seating' => '3',
		    'stands' => '0',
		    'color' => 'stříbrná',
		    'power_weigth' => null,
		    'maximum_speed' => '250',
		);
		$return = $this->model->addVehicle(2, $info);
		Assert::equal($this->database->table('vehicles')->count(), 4);
		$row = $this->database->table('vehicles')->get($return->id);
		Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
		Assert::equal(2, $row->users_id);
		Assert::equal('Auto4', $row->name);
		Assert::equal('popis auta4', $row->desc);
		Assert::equal('132as40', $row->registration_number);
		Assert::equal(5, $row->vehicle_make);
		Assert::equal('typ', $row->vehicle_type);
		Assert::equal('obchod', $row->commercial_description);
		Assert::equal('aasdzxc13260004', $row->vin);
		Assert::equal(2400, $row->mass_permissible);
		Assert::equal(2004, $row->mass_operating);
		Assert::equal(2504, $row->engine_capacity);
		Assert::equal(null, $row->engine_max_net_power);
		Assert::equal('nafta', $row->engine_fuel);
		Assert::equal(3, $row->seating);
		Assert::equal(0, $row->stands);
		Assert::equal('stříbrná', $row->color);
		Assert::equal(null, $row->power_weigth);
		Assert::equal('250', $row->maximum_speed);
		Assert::equal(0.0, $row->mileage);
	}
	
	function testAddVehicle_wrongData()
	{
	    $model = $this->model;
	    
	    Assert::exception(function() use ($model){
		$info = array(
		    'unit_id' => 105,
		    'name' => 'Auto4',
		    'desc' => 'popis auta4',
		    'registration_number' => '132as40123',
		    'type' => 5,
		    'vehicle_make' => 8,
		    'vehicle_type' => 3,
		    'commercial_description' => 'obchod',
		    'vin' => 'aasdzxc13260004',
		    'mass_permissible' => '2400',
		    'mass_operating' => '2004',
		    'engine_capacity' => '2504',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'nafta',
		    'seating' => '3',
		    'stands' => '0',
		    'color' => 'stříbrná',
		    'power_weigth' => null,
		    'maximum_speed' => '250',
		);
		$model->addVehicle(2, $info); // příliš dlouhé registrační číslo
	    }, 'PDOException');
	    
	    Assert::exception(function() use ($model){
		$info = array(
		    'unit_id' => 105,
		    'name' => 'Auto4',
		    'desc' => 'popis auta4',
		    'registration_number' => '132as40',
		    'type' => 4,
		    'vehicle_make' => 7,
		    'vehicle_type' => 3,
		    'commercial_description' => 'obchod',
		    'vin' => 'aasdzxc13260004',
		    'mass_permissible' => '2400',
		    'mass_operating' => '2004',
		    'engine_capacity' => '2504',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'nafta',
		    'seating' => '3',
		    'stands' => '0',
		    'color' => 'stříbrná',
		    'power_weigth' => null,
		    'maximum_speed' => '250',
		);
		$model->addVehicle(5, $info); // neexistující vlastník
	    }, 'PDOException');
	}
	
	function testGetUsersVehicles()
	{
	    $vehicles = new \Model\Vehicles($this->database);
	    $row = $vehicles->getUsersVehicles(1);
	    Assert::true($row instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $row->count());
	    Assert::equal(1, $row[1]->id);
	    
	    $row = $vehicles->getUsersVehicles(2);
	    Assert::true($row instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $row->count());
	    Assert::equal(3, $row->fetch()->id);
	    
	}
	
	function testGetUsersVehicles_notExistUser() {
	    $row = $this->model->getUsersVehicles(8);
	    Assert::true($row instanceof \Nette\Database\Table\Selection);
	    Assert::equal(0, $row->count());
	}
	
	function testModifyVehicle() {
	    $info = array(
		    'unit_id' => 105,
		    'name' => 'Auto4',
		    'desc' => 'popis auta4',
		    'registration_number' => '132as40',
		    'type' => 4,
		    'vehicle_make' => 11,
		    'vehicle_type' => 'typ',
		    'commercial_description' => 'obchod',
		    'vin' => 'aasdzxc13260004',
		    'mass_permissible' => '2400',
		    'mass_operating' => '2004',
		    'engine_capacity' => '2504',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'nafta',
		    'seating' => '3',
		    'stands' => '0',
		    'color' => 'stříbrná',
		    'power_weigth' => null,
		    'maximum_speed' => '250',
		);
	    $updated = $this->model->modifyVehicle(1, $info);
	    Assert::true($updated);
	    $row = $this->database->table('vehicles')->get(1);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1, $row->users_id);
	    Assert::equal('Auto4', $row->name);
	    Assert::equal('popis auta4', $row->desc);
	    Assert::equal('132as40', $row->registration_number);
	    Assert::equal(11, $row->vehicle_make);
	    Assert::equal('typ', $row->vehicle_type);
	    Assert::equal('obchod', $row->commercial_description);
	    Assert::equal('aasdzxc13260004', $row->vin);
	    Assert::equal(2400, $row->mass_permissible);
	    Assert::equal(2004, $row->mass_operating);
	    Assert::equal(2504, $row->engine_capacity);
	    Assert::equal(null, $row->engine_max_net_power);
	    Assert::equal('nafta', $row->engine_fuel);
	    Assert::equal(3, $row->seating);
	    Assert::equal(0, $row->stands);
	    Assert::equal('stříbrná', $row->color);
	    Assert::equal(null, $row->power_weigth);
	    Assert::equal('250', $row->maximum_speed);
	    Assert::equal(0.0, $row->mileage);
	    
	}
	
	function testModifyVehicle_wrongData()
	{
	    $model = $this->model;
	    
	    
	    Assert::exception(function() use ($model){
		$info = array(
		    'unit_id' => 105,
		    'name' => 'Auto4',
		    'desc' => 'popis auta4',
		    'registration_number' => '132as40asd',
		    'type' => 4,
		    'vehicle_make' => 6,
		    'vehicle_type' => 'typ',
		    'commercial_description' => 'obchod',
		    'vin' => 'aasdzxc13260004',
		    'mass_permissible' => '2400',
		    'mass_operating' => '2004',
		    'engine_capacity' => '2504',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'nafta',
		    'seating' => '3',
		    'stands' => '0',
		    'color' => 'stříbrná',
		    'power_weigth' => null,
		    'maximum_speed' => '250',
		);
		$model->modifyVehicle(2, $info); // příliš dlouhé registrační číslo
	    }, 'PDOException');
	    
	}
	
	function testGetVehiclesState() {
	    $state = $this->model->getVehiclesState(2);
	    Assert::equal(0, $state);
	}
	
	function testMakeVehicleActive() {
	    $this->model->makeVehicleActive(2);
	    $state = $this->model->getVehiclesState(2);
	    Assert::equal(1, $state);
	}
	
	function testMakeVehicleInactive() {
	    $this->model->makeVehicleInactive(1);
	    $state = $this->model->getVehiclesState(1);
	    Assert::equal(0, $state);
	}
	
	function testChangeVehiclePic() {
	    $updated = $this->model->changeVehiclePic(2, 'zmenena.jpg');
	    Assert::true($updated);
	    $row = $this->database->table('vehicles')->get(2);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal('zmenena.jpg', $row->profilpic);
	    Assert::equal(2, $row->id);
	    Assert::equal('Auto2', $row->name);
	    Assert::equal('popis auta2', $row->desc);
	    Assert::equal('321asd6', $row->registration_number);
	    Assert::equal(10, $row->vehicle_make);
	    Assert::equal('1', $row->vehicle_type);
	    Assert::equal('moto', $row->commercial_description);
	    Assert::equal('aasdzxc13265400', $row->vin);
	    Assert::equal(1000, $row->mass_permissible);
	    Assert::equal(500, $row->mass_operating);
	    Assert::equal(1000, $row->engine_capacity);
	    Assert::equal('500', $row->engine_max_net_power);
	    Assert::equal('BA 95 B', $row->engine_fuel);
	    Assert::equal(2, $row->seating);
	    Assert::equal(0, $row->stands);
	    Assert::equal('černá', $row->color);
	    Assert::equal('12', $row->power_weigth);
	    Assert::equal('250', $row->maximum_speed);
	    Assert::equal(40.0, $row->mileage);
	}
	
	function testChangeOwner() {
	    $updated = $this->model->changeOwner(1, 2);
	    Assert::true($updated);
	    $row = $this->database->table('vehicles')->get(1);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal('auto1.jpg', $row->profilpic);
	    Assert::equal(1, $row->id);
	    Assert::equal(2, $row->users_id);
	}
	
	function testChangeOwner_notExistsUser() {
	    $model = $this->model;
	    Assert::exception(function() use ($model){
		$model->changeOwner(1, 5); // neexistující vlastník
	    }, 'PDOException');
	}
	
	function testGetVehicle() {
	    $row = $this->model->getVehicle(2);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal('auto2.jpg', $row->profilpic);
	    Assert::equal(2, $row->id);
	    Assert::equal('Auto2', $row->name);
	    Assert::equal('popis auta2', $row->desc);
	    Assert::equal('321asd6', $row->registration_number);
	    Assert::equal(10, $row->vehicle_make);
	    Assert::equal('1', $row->vehicle_type);
	    Assert::equal('moto', $row->commercial_description);
	    Assert::equal('aasdzxc13265400', $row->vin);
	    Assert::equal(1000, $row->mass_permissible);
	    Assert::equal(500, $row->mass_operating);
	    Assert::equal(1000, $row->engine_capacity);
	    Assert::equal('500', $row->engine_max_net_power);
	    Assert::equal('BA 95 B', $row->engine_fuel);
	    Assert::equal(2, $row->seating);
	    Assert::equal(0, $row->stands);
	    Assert::equal('černá', $row->color);
	    Assert::equal('12', $row->power_weigth);
	    Assert::equal('250', $row->maximum_speed);
	    Assert::equal(40.0, $row->mileage);
	}
	
	function testGetVehicle_notExists() {
	    $row = $this->model->getVehicle(8);
	    Assert::null($row);
	}
	
	function testGetVehiclesTypes() {
	    $row = $this->model->getVehiclesTypes();
	    Assert::equal(10, count($row));
	}
	
	function testGetVehiclesMake() {
	    $row = $this->model->getVehiclesMake();
	    Assert::equal(15, count($row));
	}
	
	function testGetDrivers() {
	    $id = 1;
	    $return = $this->model->getDrivers($id);
	    Assert::equal(1, count($return));
	    Assert::equal(2, $return->fetch()->user_id);
	}
	
	function testAddDriver() {
	    $id = 1;
	    $user_id = 3;
	    $return = $this->model->addDriver($id, $user_id);
	    Assert::equal(3, $this->database->table('vehicle_drivers')->count());
	}
	
	function testRemoveDriver() {
	    $id = 1;
	    $this->model->removeDriver($id);
	    Assert::equal(1, $this->database->table('vehicle_drivers')->count());
	}
	
	function testIsRentPossible_false() {
	    $id = 2;
	    $from = '2014-04-16';
	    $to = '2014-04-21';
	    Assert::false($this->model->isRentPossible($id, $from, $to));
	}
	
	function testIsRentPossible_true() {
	    $id = 2;
	    $from = '2014-04-16';
	    $to = '2014-04-18';
	    Assert::true($this->model->isRentPossible($id, $from, $to));
	}
	
	function testGetRent() {
	    $id = 2;
	    $return = $this->model->getRent($id);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $return->count());
	}
	
	function testAddRent() {
	    $id = 2;
	    $user_id = 3;
	    $from = '2014-04-16';
	    $to = '2014-04-18';
	    $return = $this->model->addRent($id, $user_id, $from, $to);
	    Assert::equal(2, $this->database->table('vehicle_rent')->count());
	    $row = $this->database->table('vehicle_rent')->wherePrimary($return->id)->fetch();
	    Assert::equal($user_id, $row->user_id);
	    Assert::equal($id, $row->vehicle_id);
	}
	
	function testAddRent_exception() {
	    $model = $this->model;
	    Assert::exception(function() use ($model) {
		$id = 2;
		$user_id = 3;
		$from = '2014-04-16';
		$to = '2014-04-21';
		$return = $this->model->addRent($id, $user_id, $from, $to);
	    }, 'Model\AlreadyExistDateRangeException');
	}
	
	function testGetRentByDate() {
	    $id = 2;
	    $date = '2014-04-20';
	    $return = $this->model->getRentByDate($id, $date);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $return->count());
	}
	
	function testGetRentedVehicles() {
	    $user_id = 1;
	    $return = $this->model->getRentedVehicles($user_id);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(0, $return->count());
	}
	
	function testGetOldRentedVehicles() {
	    $user_id = 1;
	    $return = $this->model->getOldRentedVehicles($user_id);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $return->count());
	}
	
	function testGetUsesVehicles() {
	    $user_id = 1;
	    $return = $this->model->getUsesVehicles($user_id);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $return->count());
	}
	
	function testGetInactiveVehicles() {
	    $user_id = 1;
	    $return = $this->model->getInactiveVehicles($user_id);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $return->count());
	}
	
	

}


id(new VehiclesTest($container))->run();