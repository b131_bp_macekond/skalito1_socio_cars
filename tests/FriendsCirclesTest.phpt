<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class FriendsCirclesTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\FriendsCircles */
	private $model;
 
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\FriendsCircles');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
	    
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('users')->delete();
		$this->database->table('friends')->delete();
		$this->database->table('friends_circles')->delete();
		$this->database->table('friends_to_circles')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 3,
		    'email' => 'user3@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf3',
		    'salt' => 'qwertqwertqwertqwer3',
		    'name' => 'User3',
		    'surname' => 'Usersky3',
		    'sex' => 'female',
		    'city' => 'Prague3',
		    'profilpic' => 'obrazek3.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 4,
		    'email' => 'user4@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf4',
		    'salt' => 'qwertqwertqwertqwer4',
		    'name' => 'User4',
		    'surname' => 'Usersky4',
		    'sex' => 'female',
		    'city' => 'Prague4',
		    'profilpic' => 'obrazek4.jpg',
		));
		
		$this->database->table('friends')->insert(array(
		    'id' => 1,
		    'from' => 1,
		    'to' => 3,
		    'accepted' => 1,
		));
		$this->database->table('friends')->insert(array(
		    'id' => 2,
		    'from' => 4,
		    'to' => 1,
		    'accepted' => 1,
		));
		$this->database->table('friends')->insert(array(
		    'id' => 3,
		    'from' => 2,
		    'to' => 3,
		    'accepted' => 0,
		));
		$this->database->table('friends')->insert(array(
		    'id' => 4,
		    'from' => 2,
		    'to' => 4,
		    'accepted' => 1,
		));
		
		$this->database->table('friends_circles')->insert(array(
		    'id' => 1,
		    'user_id' => 1,
		    'name' => 'Kruh 1',
		));
		
		$this->database->table('friends_circles')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'name' => 'Kruh 2',
		));
		
		$this->database->table('friends_circles')->insert(array(
		    'id' => 3,
		    'user_id' => 3,
		    'name' => 'Kruh 3',
		));
		$this->database->table('friends_to_circles')->insert(array(
		    'id' => 1,
		    'friend_id' => 1,
		    'users_id' => 3,
		    'friends_circle_id' => 1
		));
		$this->database->table('friends_to_circles')->insert(array(
		    'id' => 2,
		    'friend_id' => 1,
		    'users_id' => 1,
		    'friends_circle_id' => 3
		));
		$this->database->table('friends_to_circles')->insert(array(
		    'id' => 3,
		    'friend_id' => 2,
		    'users_id' => 4,
		    'friends_circle_id' => 1
		));
	}
 
	function testGetUsersCircles1()
	{
		$row = $this->model->getUsersCircles(1);
		Assert::true($row instanceof \Nette\Database\Table\Selection);
		Assert::equal($row->count(), 2);
	}
	
	function testGetUsersCircles2()
	{
		$row = $this->model->getUsersCircles(2);
		Assert::true($row instanceof \Nette\Database\Table\Selection);
		Assert::equal($row->count(), 0);
	}
	
	function testGetFriendsInCircle1()
	{
		$row = $this->model->getFriendsInCircle(1);
		Assert::true($row instanceof \Nette\Database\Table\Selection);
		Assert::equal($row->count(), 2);
	}
	
	function testGetFriendsInCircle2()
	{
		$row = $this->model->getFriendsInCircle(2);
		Assert::true($row instanceof \Nette\Database\Table\Selection);
		Assert::equal($row->count(), 0);
	}
	
	function testGetFriendsInCircle3()
	{
		$row = $this->model->getFriendsInCircle(3);
		Assert::true($row instanceof \Nette\Database\Table\Selection);
		Assert::equal($row->count(), 1);
	}
	
	function testGetSingleCircle1() 
	{
	    $row = $this->model->getSingleCircle(3, 3);
	    Assert::true($row instanceof Nette\Database\Table\ActiveRow);
	    Assert::equal($row->id, 3);
	    Assert::equal($row->name, 'Kruh 3');
	}
	
	function testGetSingleCircle2() 
	{
	    $row = $this->model->getSingleCircle(2, 1);
	    Assert::true($row instanceof Nette\Database\Table\ActiveRow);
	    Assert::equal(2, $row->id);
	    Assert::equal('Kruh 2', $row->name);
	}
	
	function testGetSingleCircle_wrongData() 
	{
	    $model = $this->model;
	    Assert::exception(function() use ($model){
		$row = $model->getSingleCircle(3, 1);	// wrong user
	    }, 'Model\CircleDoesNotExistsException');
	}
	
	function testAddCircle()
	{
	    $return = $this->model->addCircle(1, 'Další kruh');
	    Assert::equal($this->database->table('friends_circles')->count(), 4);
	    $row = $this->database->table('friends_circles')->get($return->id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1, $row->user_id);
	    Assert::equal('Další kruh', $row->name);
	}
	
	function testAddCircle_wrongData()
	{
	    
	    $model = $this->model;
	    Assert::exception(function() use ($model){
		$return = $model->addCircle(6, 'Další kruh'); // wrong user
	    }, 'PDOException');
	}
	
	function testEditCircle()
	{
	    $updated = $this->model->editCircle(1, 1, 'Kruh upravený', array(4));
	    Assert::true($updated);
	    $row = $this->database->table('friends_circles')->get(1);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1, $row->user_id);
	    Assert::equal('Kruh upravený', $row->name);
	    $friends = $this->database->table('friends_to_circles')->where('friends_circle_id', 1);
	    Assert::true($friends instanceof \Nette\Database\Table\Selection);
	    Assert::equal(1, $friends->count());
	}
	
	function testEditCircle_wrongData()
	{
	    $model = $this->model;
	    Assert::exception(function() use ($model){
		$return = $model->editCircle(1, 6, 'Další kruh', array()); // wrong user
	    }, 'Model\CircleDoesNotExistsException');
	}
	
	function testDeleteCircle() {
	    $this->model->deleteCircle(1, 1);
	    $row = $this->database->table('friends_circles')->get(1);
	    Assert::false($row);
	}
	
	function testDeleteCircle_wrongData() {
	    $model = $this->model;
	    Assert::exception(function() use ($model) {
		 $this->model->deleteCircle(20, 1); // wrong circle
	    }, 'Model\CircleDoesNotExistsException');
	}

}


id(new FriendsCirclesTest($container))->run();