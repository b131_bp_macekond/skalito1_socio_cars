<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class GroupsTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\Groups */
	private $model;
 
	
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\Groups');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
		
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('groups_comments')->delete();
		$this->database->table('groups_wall')->delete();
		$this->database->table('groups_members')->delete();
		$this->database->table('groups')->delete();
		$this->database->table('users')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 3,
		    'email' => 'user3@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf3',
		    'salt' => 'qwertqwertqwertqwer3',
		    'name' => 'User3',
		    'surname' => 'Usersky3',
		    'sex' => 'female',
		    'city' => 'Prague3',
		    'profilpic' => 'obrazek3.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 4,
		    'email' => 'user4@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf4',
		    'salt' => 'qwertqwertqwertqwer4',
		    'name' => 'User4',
		    'surname' => 'Usersky4',
		    'sex' => 'female',
		    'city' => 'Prague4',
		    'profilpic' => 'obrazek4.jpg',
		));
		
		$this->database->table('groups')->insert(array(
		    'id' => 1,
		    'user_id' => 1,
		    'name' => 'Group1',
		    'desc' => 'description1',
		));
		
		$this->database->table('groups')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'name' => 'Group2',
		    'desc' => 'description2',
		));
		
		$this->database->table('groups')->insert(array(
		    'id' => 3,
		    'user_id' => 4,
		    'name' => 'Group3',
		    'desc' => 'description3',
		));
		
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 1,
		    'user_id' => 1,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 1,
		    'user_id' => 2,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 1,
		    'user_id' => 4,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 2,
		    'user_id' => 2,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 2,
		    'user_id' => 1,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 3,
		    'user_id' => 4,
		));
		$this->database->table('groups_members')->insert(array(
		    'group_id' => 3,
		    'user_id' => 2,
		));
		
		$this->database->table('groups_wall')->insert(array(
		    'id' => 1,
		    'group_id' => 1,
		    'user_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 1',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 2,
		    'group_id' => 1,
		    'user_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 1',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 3,
		    'group_id' => 1,
		    'user_id' => 2,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 2',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 4,
		    'group_id' => 3,
		    'user_id' => 4,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 3',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 5,
		    'group_id' => 3,
		    'user_id' => 2,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 4',
		));
		$this->database->table('groups_wall')->insert(array(
		    'id' => 6,
		    'group_id' => 1,
		    'user_id' => 4,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Vzkaz 5',
		));
		
		$this->database->table('groups_comments')->insert(array(
		    'id' => 1,
		    'user_id' => 2,
		    'groups_wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 1',
		));
		$this->database->table('groups_comments')->insert(array(
		    'id' => 2,
		    'user_id' => 1,
		    'groups_wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 2',
		));
		$this->database->table('groups_comments')->insert(array(
		    'id' => 3,
		    'user_id' => 4,
		    'groups_wall_id' => 5,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 3',
		));
		$this->database->table('groups_comments')->insert(array(
		    'id' => 4,
		    'user_id' => 2,
		    'groups_wall_id' => 1,
		    'date' => new \Nette\Database\SqlLiteral('NOW()'),
		    'content' => 'Komentar 4',
		));
	}
 
	function testAddGroup() {
		$user_id = 1;
		$name = "Nová grupa";
		$desc = "Popis nové grupy";
		$return = $this->model->addGroup($user_id, $name, $desc);
		Assert::equal(4, $this->database->table('groups')->count());
		$row = $this->database->table('groups')->get($return->id);
		Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
		Assert::equal($user_id, $row->user_id);
		Assert::equal($name, $row->name);
		Assert::equal($desc, $row->desc);
		$row = $this->database->table('groups_members')->where(array('group_id' => $return->id, 'user_id' => $user_id));
		Assert::true($row->count()>0);
	}
	
	function testAddGroup_wrongData() {
		$model = $this->model;
		Assert::exception(function() use ($model) {
		    $user_id = 10;
		    $name = "Nová grupa";
		    $desc = "Popis nové grupy";
		    $return = $this->model->addGroup($user_id, $name, $desc);
		}, 'PDOException');
	}
	
	function testModifyGroup() {
	    $group_id = 1;
	    $name = "Nové jmeno";
	    $desc = "Nový popis";
	    $this->model->modifyGroup($group_id, $name, $desc);
	    $row = $this->database->table('groups')->get($group_id);
	    Assert::equal(3, $this->database->table('groups')->count());
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1, $row->user_id);
	    Assert::equal($name, $row->name);
	    Assert::equal($desc, $row->desc);
	}
	
	function testSearchGroups_byName() {
	    $q = "group1";
	    $res = $this->model->searchGroups($q);
	    Assert::equal(1, $res->count());
	    Assert::equal("Group1", $res->fetch()->name);
	}
	
	function testSearchGroups_byDesc() {
	    $q = "description3";
	    $res = $this->model->searchGroups($q);
	    Assert::equal(1, $res->count());
	    Assert::equal("Group3", $res->fetch()->name);
	}
	
	function testIsMember() {
	    $group_id = 1;
	    $user_id = 1;
	    Assert::true($this->model->isMember($group_id, $user_id));
	}
	
	function testIsMember_Not() {
	    $group_id = 4;
	    $user_id = 1;
	    Assert::false($this->model->isMember($group_id, $user_id));
	}
	
	function testAddMember() {
	    $group_id = 3;
	    $user_id = 1;
	    $this->model->addMember($group_id, $user_id);
	    $row = $this->database->table('groups_members')->where(array('group_id' => $group_id, 'user_id' => $user_id));
	    Assert::true($row->count()>0);
	}
	
	function testRemoveMember() {
	    $group_id = 1;
	    $user_id = 2;
	    $this->model->removeMember($group_id, $user_id);
	    
	    $row = $this->database->table('groups_members')->where(array('group_id' => $group_id, 'user_id' => $user_id));
	    Assert::false($row->count()>0);
	}
	
	function testGetMembers() {
	    $group_id = 1;
	    $row = $this->model->getMembers($group_id);
	    Assert::true($row instanceof \Nette\Database\Table\Selection);
	    Assert::equal(3, $row->count());
	}
	
	function testGetUsersGroup() {
	    $user_id = 1;
	    $row = $this->model->getUsersGroup($user_id);
	    Assert::equal(2, count($row));
	}
	
	function testGetGroupWall() {
	    $group_id = 3;
	    $row = $this->model->getGroupWall($group_id);
	    Assert::true($row instanceof \Nette\Database\Table\Selection);
	    Assert::equal(2, $row->count());
	}
	
	function testAddWallPost() {
	    $group_id = 3;
	    $user_id = 1;
	    $content = "Nový vzkaz";
	    $result = $this->model->addWallPost($group_id, $user_id, $content);
	    Assert::equal(7, $this->database->table('groups_wall')->count());
	    $row = $this->database->table('groups_wall')->get($result->id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal($user_id, $row->user_id);
	    Assert::equal($group_id, $row->group_id);
	    Assert::equal($content, $row->content);
	}
	
	function testAddComment() {
	    $wall_id = 3;
	    $user_id = 1;
	    $content = "Nový komentář";
	    $result = $this->model->addComment($wall_id, $user_id, $content);
	    Assert::equal(5, $this->database->table('groups_comments')->count());
	    $row = $this->database->table('groups_comments')->get($result->id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal($user_id, $row->user_id);
	    Assert::equal($wall_id, $row->groups_wall_id);
	    Assert::equal($content, $row->content);
	}

}


id(new GroupsTest($container))->run();