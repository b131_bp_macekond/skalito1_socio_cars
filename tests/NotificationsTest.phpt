<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class NotificationsTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\Notifications */
	private $model;
 
	
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\Notifications');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('notifications')->delete();
		$this->database->table('friends')->delete();
		$this->database->table('users')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		
		$this->database->table('notifications')->insert(array(
		    'id' => 1,
		    'users_id' => 1,
		    'time' => '2013-12-11 10:23:45',
		    'link' => 'User:',
		    'link_params' => json_encode(array('id'=>2)),
		    'message' => 'Uživatel {person} potvrdil vaší žádost o přátelství',
		    'attr' => json_encode(array(2)),
		    'seen' => 0,
		));
		
		$this->database->table('notifications')->insert(array(
		    'id' => 2,
		    'users_id' => 1,
		    'time' => '2013-12-12 10:23:45',
		    'link' => 'User:',
		    'link_params' => json_encode(array('id'=>2)),
		    'message' => 'Uživatel {person} potvrdil vaší žádost o přátelství',
		    'attr' => json_encode(array(2)),
		    'seen' => 1,
		));
		
		$this->database->table('notifications')->insert(array(
		    'id' => 3,
		    'users_id' => 2,
		    'time' => '2013-12-12 10:23:45',
		    'link' => 'User:default',
		    'link_params' => json_encode(array('id'=>2)),
		    'message' => 'Uživatel {person} okomentoval váš post',
		    'attr' => json_encode(array(1)),
		));
		
		
	}
	
	function testAddNotification(){
	    $userid = 2;
	    $link = 'User:';
	    $link_params = array('id'=>1);
	    $message = "Uživatel {person} váš požádat o přátelství";
	    $attr = array(1);
	    $return = $this->model->addNotification($userid, $link, $link_params, $message, $attr);
	    Assert::equal(4, $this->database->table('notifications')->count());
	    $row = $this->database->table('notifications')->get($return->id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal($userid, $row->users_id);
	    Assert::equal($link, $row->link);
	    Assert::equal(json_encode($link_params), $row->link_params);
	    Assert::equal($message, $row->message);
	    Assert::equal(json_encode($attr), $row->attr);
	    
	}
	
	function testGetUnseenNotificationsCount() {
	    $userId = 1;
	    $return = $this->model->getUnseenNotificationsCount($userId);
	    Assert::equal(1, $return);
	}
	
	function testGetNotifications() {
	    $userId = 1;
	    $return = $this->model->getNotifications($userId);
	    Assert::equal(2, count($return));
	    Assert::equal(0, $this->model->getUnseenNotificationsCount($userId));
	}
	

}


id(new NotificationsTest($container))->run();