<?php

namespace Test;

use Nette,
        Tester,
        Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';


class RoutesTest extends Tester\TestCase
{
        private $container;


        /** @var \Model\Routes */
	private $model;
 
	
	/** @var Nette\Database\Context */
	private $database;
 
	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
		$this->model = $container->getByType('Model\Routes');
		$this->database = $container->getByType('Nette\Database\Context');
	}
 
	function setUp()
	{
		
		Tester\Environment::lock(__DIR__ . '/tmp/database.lock');
		$this->database->table('groups_comments')->delete();
		$this->database->table('groups_wall')->delete();
		$this->database->table('groups_members')->delete();
		$this->database->table('groups')->delete();
		$this->database->table('users')->delete();
		$this->database->table('entries')->delete();
		$this->database->table('route_users')->delete();
		$this->database->table('routes')->delete();
		$this->database->table('vehicle_drivers')->delete();
		$this->database->table('vehicle_rent')->delete();
		$this->database->table('vehicles')->delete();
		$this->database->table('users')->delete();
		$this->database->table('users')->insert(array(
		    'id' => 1,
		    'email' => 'user@user.cz',
		    'password' => 'asdfgasdfgasdfgasdfg',
		    'salt' => 'qwertqwertqwertqwert',
		    'name' => 'User',
		    'surname' => 'Usersky',
		    'sex' => 'male',
		    'city' => 'Prague',
		    'profilpic' => 'obrazek.jpg',
		));
		
		$this->database->table('users')->insert(array(
		    'id' => 2,
		    'email' => 'user2@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf2',
		    'salt' => 'qwertqwertqwertqwer2',
		    'name' => 'User2',
		    'surname' => 'Usersky2',
		    'sex' => 'female',
		    'city' => 'Prague2',
		    'profilpic' => 'obrazek2.jpg',
		));
		
		$this->database->table('users')->insert(array(
		    'id' => 3,
		    'email' => 'user3@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf3',
		    'salt' => 'qwertqwertqwertqwer3',
		    'name' => 'User3',
		    'surname' => 'Usersky3',
		    'sex' => 'female',
		    'city' => 'Prague3',
		    'profilpic' => 'obrazek3.jpg',
		));
		$this->database->table('users')->insert(array(
		    'id' => 4,
		    'email' => 'user4@user.cz',
		    'password' => 'asdfgasdfgasdfgasdf4',
		    'salt' => 'qwertqwertqwertqwer4',
		    'name' => 'User4',
		    'surname' => 'Usersky4',
		    'sex' => 'female',
		    'city' => 'Prague4',
		    'profilpic' => 'obrazek4.jpg',
		));
		
		$this->database->table('vehicles')->insert(array(
		    'id' => 1,
		    'users_id' => 1,
		    'unit_id' => 1234,
		    'active' => 1,
		    'name' => 'Auto1',
		    'profilpic' => 'auto1.jpg',
		    'desc' => 'popis auta',
		    'registration_number' => '132asd6',
		    'type' => 1,
		    'vehicle_make' => 5,
		    'vehicle_type' => '2.0',
		    'commercial_description' => 'mondeo',
		    'vin' => 'aasdzxc13265498',
		    'mass_permissible' => '2000',
		    'mass_operating' => '1500',
		    'engine_capacity' => '1988',
		    'engine_max_net_power' => null,
		    'engine_fuel' => 'BA 95 B',
		    'seating' => '5',
		    'stands' => '0',
		    'color' => 'červena-metal',
		    'power_weigth' => null,
		    'maximum_speed' => '199',
		    'mileage' => 0,
		));
		$this->database->table('vehicles')->insert(array(
		    'id' => 2,
		    'users_id' => 1,
		    'unit_id' => 1200,
		    'active' => 0,
		    'name' => 'Auto2',
		    'profilpic' => 'auto2.jpg',
		    'desc' => 'popis auta2',
		    'registration_number' => '321asd6',
		    'type' => 2,
		    'vehicle_make' => 10,
		    'vehicle_type' => '1',
		    'commercial_description' => 'moto',
		    'vin' => 'aasdzxc13265400',
		    'mass_permissible' => '1000',
		    'mass_operating' => '500',
		    'engine_capacity' => '1000',
		    'engine_max_net_power' => '500',
		    'engine_fuel' => 'BA 95 B',
		    'seating' => '2',
		    'stands' => '0',
		    'color' => 'černá',
		    'power_weigth' => '12',
		    'maximum_speed' => '250',
		    'mileage' => 40,
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 1,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '01:57:30',
		    'length' => '94.04',
		    'start_lat' => '49.73',
		    'start_long' => '13.37',
		    'end_lat' => '50.07',
		    'end_long' => '14.43',
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 2,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Zbečno - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '03:42:45',
		    'length' => 115.16,
		    'start_lat' => 49.73,
		    'start_long' => 13.37,
		    'end_lat' => 50.07,
		    'end_long' => 14.43,
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 3,
		    'vehicles_id' => 1,
		    'users_id' => 2,
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 4,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Zbečno - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '03:42:45',
		    'length' => 115.16,
		    'start_lat' => 52,
		    'start_long' => 17,
		    'end_lat' => 50.07,
		    'end_long' => 14.43,
		));
		
		$this->database->table('routes')->insert(array(
		    'id' => 5,
		    'vehicles_id' => 2,
		    'users_id' => 4,
		    'name' => 'Plzeň - Zbečno - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '03:42:45',
		    'length' => 115.16,
		    'start_lat' => 52,
		    'start_long' => 17,
		    'end_lat' => 50.07,
		    'end_long' => 14.43,
		));
		$this->database->table('routes')->insert(array(
		    'id' => 6,
		    'vehicles_id' => 1,
		    'users_id' => 1,
		    'name' => 'Plzeň - Zbečno - Praha',
		    'start_time' => '2014-04-20 15:16:32',
		    'duration' => '03:42:45',
		    'length' => 115.16,
		    'start_lat' => 52,
		    'start_long' => 17,
		    'end_lat' => 50.07,
		    'end_long' => 14.43,
		));
		
		
		$this->database->table('entries')->insert(array(
		    'id' => 1,
		    'routes_id' => 1,
		    'longitude' => 49.73,
		    'latitude' => 13.37,
		    'timestamp' => '2014-04-20 15:16:32',
		    'odometer' => 12,
		    'velocity' => 15,
		    'consumption' => 10.2,
		    'fuel_remaining' => 12.7,
		    'altitude' => 0,
		    'engine_temp' => 25,
		    'engine_rpm' => 1600,
		    'throttle' => 12,
		));
		$this->database->table('entries')->insert(array(
		    'id' => 2,
		    'routes_id' => 1,
		    'longitude' => 50.07,
		    'latitude' => 14.43,
		    'timestamp' => '2014-04-20 15:17:02',
		    'odometer' => 15,
		    'velocity' => 16,
		    'consumption' => 12.2,
		    'fuel_remaining' => 10.8,
		    'altitude' => 10,
		    'engine_temp' => 50,
		    'engine_rpm' => 1900,
		    'throttle' => 13,
		));
		
		$this->database->table('entries')->insert(array(
		    'id' => 3,
		    'routes_id' => 2,
		    'longitude' => 49.73,
		    'latitude' => 13.37,
		    'timestamp' => '2014-04-20 15:16:32',
		    'odometer' => 12,
		    'velocity' => 15,
		    'consumption' => 10.2,
		    'fuel_remaining' => 12.7,
		    'altitude' => 0,
		    'engine_temp' => 25,
		    'engine_rpm' => 1600,
		    'throttle' => 12,
		));
		$this->database->table('entries')->insert(array(
		    'id' => 4,
		    'routes_id' => 2,
		    'longitude' => 49.97,
		    'latitude' => 14.00,
		    'timestamp' => '2014-04-20 15:17:02',
		    'odometer' => 15,
		    'velocity' => 16,
		    'consumption' => 12.2,
		    'fuel_remaining' => 10.8,
		    'altitude' => 10,
		    'engine_temp' => 50,
		    'engine_rpm' => 1900,
		    'throttle' => 13,
		));
		$this->database->table('entries')->insert(array(
		    'id' => 5,
		    'routes_id' => 2,
		    'longitude' => 50.07,
		    'latitude' => 14.43,
		    'timestamp' => '2014-04-20 15:17:32',
		    'odometer' => 15,
		    'velocity' => 16,
		    'consumption' => 12.2,
		    'fuel_remaining' => 10.8,
		    'altitude' => 10,
		    'engine_temp' => 50,
		    'engine_rpm' => 1900,
		    'throttle' => 13,
		));
		
		$this->database->table('route_users')->insert(array(
		    'routes_id' => 1,
		    'users_id' => 2,
		));
		
		$this->database->table('vehicle_rent')->insert(array(
		    'user_id' => 1,
		    'vehicle_id' => 2,
		    'from' => '2014-04-20 12:16:32',
		    'to' => '2014-04-20 17:16:32',
		));
	}
 
	function testAddRoute() {
		$vehicleId = 1;
		$userId = 2;
		$return = $this->model->addRoute($vehicleId, $userId);
		Assert::equal(7, $this->database->table('routes')->count());
		$row = $this->database->table('routes')->get($return->id);
		Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
		Assert::equal($vehicleId, $row->vehicles_id);
		Assert::equal($userId, $row->users_id);
		Assert::equal(NULL, $row->name);
		Assert::equal(NULL, $row->start_time);
		Assert::equal(NULL, $row->duration);
		Assert::equal(NULL, $row->length);
		Assert::equal(NULL, $row->start_lat);
		Assert::equal(NULL, $row->start_long);
		Assert::equal(NULL, $row->end_lat);
		Assert::equal(NULL, $row->end_long);
	}
	
	function testInitRoute() {
	    $id = 3;
	    $start = "2014-05-12 12:00:00";
	    $end = "2014-05-12 13:00:00";
	    $start_point = array(49.79, 13.97);
	    $end_point = array(50.79, 14.97);
	    $starttime = \Nette\DateTime::from($start);
	    $endtime = \Nette\DateTime::from($end);
	    $duration = $endtime->diff($starttime);
	    $this->model->initRoute($id, $start, $end, $start_point, $end_point);
	    Assert::equal(6, $this->database->table('routes')->count());
	    $row = $this->database->table('routes')->get($id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1, $row->vehicles_id);
	    Assert::equal(2, $row->users_id);
	    Assert::equal(NULL, $row->name);
	    Assert::equal($duration->format('His'), $row->duration->format('His'));
	    Assert::equal($start_point[0], $row->start_long);
	    Assert::equal($start_point[1], $row->start_lat);
	    Assert::equal($end_point[0], $row->end_long);
	    Assert::equal($end_point[1], $row->end_lat);
	}
	
	function testCalculateRouteDistace() {
	    $points = $this->database->table('entries')->where('routes_id', 2)->order('timestamp')->select('latitude, longitude');
	    $disc = $this->model->calculateRouteDistance($points);
	    Assert::equal(123.71120501783, $disc); 
	}
	
	function testGetVehiclesRoutes() {
	    $return = $this->model->getVehiclesRoutes(1);
	    Assert::equal(5, $return->count());
	}
	
	function testGetUsersRoutes() {
	    $return = $this->model->getUsersRoutes(1);
	    Assert::equal(4, count($return));
	}
	
	function testModifyRoute() {
	    $id = 1;
	    $name = "Upravená trasa";
	    $vehicles_id = 2;
	    $sharers = array(2);
	    $return = $this->model->modifyRoute($id, $name, $vehicles_id, $sharers);
	    Assert::equal(6, $this->database->table('routes')->count());
	    $row = $this->database->table('routes')->get($id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(2, $row->vehicles_id);
	    Assert::equal($name, $row->name);
	    Assert::equal(1, $this->database->table('route_users')->where(array(
		'routes_id' => 1,
		'users_id' => 2,
	    ))->count());
	    
	}
	
	function testGetRoutesEntries() {
	    $routeId = 2;
	    $return = $this->model->getRoutesEntries($routeId);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(3, $return->count());
	}
	
	function testGetRoutePoints() {
	    $id = 2;
	    $return = $this->model->getRoutePoints($id);
	    Assert::true($return instanceof \Nette\Database\Table\Selection);
	    Assert::equal(3, $return->count());
	}
	
	function testGetRoutesStats() {
	    $id = 2;
	    $return = $this->model->getRoutesStats($id);
	    Assert::true($return instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1800.0, $return->engine_rpm);
	    Assert::equal('11.533333333333331', $return->consumption);
	    Assert::equal(12.2, $return->max_consumption);
	    Assert::equal('15.666666666666666', $return->velocity);
	    Assert::equal(16.0, $return->max_velocity);
	}
	
	function testGetSimilarRoutes() {
	    $id = 1;
	    $return = $this->model->getSimilarRoutes($id);
	    Assert::equal(1, count($return));
	    Assert::equal(2, $return[0]->id);
	}
	
	function testCreateKML() {
	    $ids = array(1 => 'blue', 2 => 'red');
	    $return = $this->model->createKML($ids);
	    Assert::matchFile($return, '<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2"><Document><Style id="blueLine"><LineStyle><color>ffff0000</color><width>4</width></LineStyle></Style><Style id="redLine"><LineStyle><color>ff0000ff</color><width>4</width></LineStyle></Style><Style id="greenLine"><LineStyle><color>ff009900</color><width>4</width></LineStyle></Style><Style id="orangeLine"><LineStyle><color>ff00ccff</color><width>4</width></LineStyle></Style><Style id="pinkLine"><LineStyle><color>ffff33ff</color><width>4</width></LineStyle></Style><Style id="brownLine"><LineStyle><color>ff66a1cc</color><width>4</width></LineStyle></Style><Style id="purpleLine"><LineStyle><color>ffcc00cc</color><width>4</width></LineStyle></Style><Style id="yellowLine"><LineStyle><color>ff61f2f2</color><width>4</width></LineStyle></Style><Placemark><name>Plzeň - Praha</name><styleUrl>#blueLine</styleUrl><LineString><coordinates>49.73,13.37,0.0
50.07,14.43,0.0</coordinates></LineString></Placemark><Placemark><name>Plzeň - Zbečno - Praha</name><styleUrl>#redLine</styleUrl><LineString><coordinates>49.73,13.37,0.0
49.97,14,0.0
50.07,14.43,0.0</coordinates></LineString></Placemark></Document></kml>
');
	    
	}
	
	function testGetRoute() {
	    $id = 1;
	    $row = $this->model->getRoute($id);
	    Assert::true($row instanceof \Nette\Database\Table\ActiveRow);
	    Assert::equal(1, $row->vehicles_id);
	    Assert::equal(1, $row->users_id);
	    Assert::equal('Plzeň - Praha', $row->name);
	    Assert::equal(94.04, $row->length);
	    Assert::equal(49.73, $row->start_lat);
	    Assert::equal(13.37, $row->start_long);
	    Assert::equal(50.07, $row->end_lat);
	    Assert::equal(14.43, $row->end_long);
	}
	
	function testGetRoute_notExists() {
	    $id = 10;
	    $row = $this->model->getRoute($id);
	    Assert::null($row);
	}

}


id(new RoutesTest($container))->run();